import { PageMeta } from './types';

export const DEFAULT_META: PageMeta = {
	title: 'Gallumon',
	description: 'Join Gallumon to earn money while having fun',
	image: 'https://pancakeswap.finance/images/hero.png',
};
