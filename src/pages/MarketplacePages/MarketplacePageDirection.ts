export enum MarketplacePageDirection {
	Root = '/marketplace',
	Preorder = '/marketplace/preorder',
	MarketplaceItem = '/marketplace/item/:id',
}

export function generateMarketPlaceItemUrl(itemId: number) {
	return `/marketplace/item/${itemId}`;
}
