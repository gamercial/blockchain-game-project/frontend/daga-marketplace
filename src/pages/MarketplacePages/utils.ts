import { Solana } from 'utils/pageDirectionConstant';

export function getSolanaMarketplaceItemUrl(itemId: number): string {
	return Solana.Marketplace.Root + `/item/${itemId}`;
}
