import React, { useState } from 'react';
import styled from 'styled-components';
import Row, { AutoRow } from 'components/Layout/Row';
import SectionLink from 'components/SectionLink';
import { majorScalePx } from 'ui-kit/utils/spacingRule/majorScalePx';
import { Egg } from 'ui-kit/components/Svg';
import { LayoutCenter } from 'ui-kit/utils';
import { PaddingXResponsive } from 'components/utils/constant';
import { MarketplacePageDirection } from './MarketplacePageDirection';

const links = [
	{
		name: 'Pre-order',
		href: MarketplacePageDirection.Preorder,
		IconComponent: <Egg width={18} />,
	},
];

const MarketplaceSubNavContainer = styled.div`
	background-color: ${({ theme }) => `${theme.colors.backgroundStandard}`};
	box-sizing: border-box;
	height: ${majorScalePx(18)};
	${PaddingXResponsive}
`;

const SubNavItemContainer = styled.div<{
	isActive?: boolean;
}>`
	border-bottom: ${({ isActive, theme }) =>
		isActive ? `3px solid ${theme.colors.primaryStandard}` : 'none'};
	height: calc(100% - 3px);
	${LayoutCenter}
`;

function MarketplaceSubNav() {
	return (
		<MarketplaceSubNavContainer>
			<Row height={'100%'}>
				<AutoRow gap={majorScalePx(1)} height={'100%'}>
					{links.map((link, index) => {
						return (
							<SubNavItemContainer key={link.name} isActive>
								<SectionLink
									IconComponent={link.IconComponent}
									href={link.href}
									displayName={link.name}
									variant={'h6'}
								/>
							</SubNavItemContainer>
						);
					})}
				</AutoRow>
			</Row>
		</MarketplaceSubNavContainer>
	);
}

export default MarketplaceSubNav;
