import styled from 'styled-components';
import { majorScalePx } from 'ui-kit/utils';
import { LeftSidebarProps } from './types';
import PreorderCountdownZone from './components/PreorderCountdownZone';

const LeftSidebarContainer = styled.div`
	background-color: ${({ theme }) => `${theme.colors.backgroundStandard}`};
	max-width: ${majorScalePx(74)};
	padding-top: ${majorScalePx(10)};
	padding-left: ${majorScalePx(6)};
	padding-right: ${majorScalePx(6)};
	width: 100%;
`;

function LeftSidebar(props: LeftSidebarProps): JSX.Element {
	return (
		<LeftSidebarContainer>
			<PreorderCountdownZone endTime={1636298732000} />
		</LeftSidebarContainer>
	);
}

export default LeftSidebar;
