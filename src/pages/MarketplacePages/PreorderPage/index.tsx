import React, { useEffect } from 'react';
import Page from 'components/Layout/Page';
import { useDagaSystemInfoContract } from 'hooks/useContract';
import EggItemSection from 'pages/sections/EggItemSection';
import MarketplaceSubNav from '../MarketplaceSubNav';
import PreorderContent from './PreorderContent';
import { MarketplacePageProps } from './types';
import { MarketplacePageDirection } from '../MarketplacePageDirection';

const Dump = (): null => null;

function EggItemContent(props: MarketplacePageProps): JSX.Element {
	if (!props.showEggItem) return <Dump />;
	return (
		<EggItemSection
			itemPath={MarketplacePageDirection.MarketplaceItem}
			rootPath={MarketplacePageDirection.Root}
		/>
	);
}

function DashboardContent(props: MarketplacePageProps): JSX.Element {
	if (props.showEggItem) return <Dump />;
	return <PreorderContent />;
}

function MarketplacePage(props: MarketplacePageProps) {
	const dagaSystemInfoContract = useDagaSystemInfoContract();
	const getTimerSale = async () => {
		const saleTimeBigNum = await dagaSystemInfoContract.getSaleOpenTime();
		console.log(saleTimeBigNum.toNumber());
	};
	useEffect(() => {
		getTimerSale();
	}, []);

	return (
		<Page>
			<MarketplaceSubNav />
			<DashboardContent {...props} />
			<EggItemContent {...props} />
		</Page>
	);
}

export default MarketplacePage;
