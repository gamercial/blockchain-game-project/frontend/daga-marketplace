import { SampleEgg } from 'ui-kit/components/Svg';
import { EggDataNode, SolanaPriceTag } from 'ui-kit/components/EggCard';
import { PreorderEggCard } from './components';

const TotalEggs = 1;

const EggItems = [...Array(TotalEggs)].map((item) => {
	const data: EggDataNode = {
		id: 'E01',
		name: 'Gold',
		priceComponent: <SolanaPriceTag solana={0.24} usd={12.6} />,
	};
	return (
		<PreorderEggCard
			eggData={data}
			eggShape={<SampleEgg height={160} width={200} />}
			eggTag={`1000 items left`}
		/>
	);
});

export { EggItems };
