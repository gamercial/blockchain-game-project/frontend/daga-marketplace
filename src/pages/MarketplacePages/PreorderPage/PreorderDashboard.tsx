import { useState } from 'react';
import styled from 'styled-components';
import { majorScalePx } from 'ui-kit/utils';
import { Typography } from 'ui-kit/components/Typography';
import { Dashboard } from 'ui-kit/components/Dashboard';
import { EggItems } from './Sample';

const PreorderDashboardContainer = styled.div`
	padding-top: ${majorScalePx(10)};
	padding-left: ${majorScalePx(10)};
	padding-right: ${majorScalePx(10)};
	width: 100%;
`;

const DashboardTitleContainer = styled.div`
	display: flex;
	margin-bottom: ${majorScalePx(10)};
`;

function PreorderDashboard(): JSX.Element {
	const [displayItems, setDisplayItems] = useState(EggItems);
	return (
		<PreorderDashboardContainer>
			<DashboardTitleContainer>
				<Typography
					variant={'h2'}
					color={'primaryStandard'}
					marginRight={majorScalePx(2)}
				>
					{displayItems.length}
				</Typography>
				<Typography variant={'h2'} color={'text'}>
					Available items
				</Typography>
			</DashboardTitleContainer>
			<Dashboard items={displayItems} itemsPerPage={12} maxColumns={6} />
		</PreorderDashboardContainer>
	);
}

export default PreorderDashboard;
