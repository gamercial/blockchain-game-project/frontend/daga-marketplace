import styled from 'styled-components';
import { useHistory } from 'react-router';
import { EggCard, EggData } from 'ui-kit/components/EggCard';
import { getSolanaMarketplaceItemUrl } from 'pages/MarketplacePages/utils';

const PreorderEggCardContainer = styled.div``;

export type PreorderEggCardProps = {
	eggData: EggData;
	eggShape: JSX.Element;
	eggTag: string;
};

function PreorderEggCard(props: PreorderEggCardProps): JSX.Element {
	const { eggData, eggShape, eggTag } = props;
	const history = useHistory();
	return (
		<PreorderEggCardContainer>
			<EggCard
				eggData={eggData}
				eggShape={eggShape}
				eggTagContent={eggTag}
				onClick={(data) => history.push(getSolanaMarketplaceItemUrl(data.id))}
			/>
		</PreorderEggCardContainer>
	);
}

export default PreorderEggCard;
