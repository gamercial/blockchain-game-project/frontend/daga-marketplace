import styled from 'styled-components';
import PreorderCountdown from 'components/Countdown/PreorderCountdown';
import { Typography } from 'ui-kit/components/Typography';
import { LayoutCenter, majorScalePx } from 'ui-kit/utils';

const PreorderCountdownZoneContainer = styled.div``;

const TitleContainer = styled.div`
	margin-bottom: ${majorScalePx(2)};
	${LayoutCenter}
`;

function PreorderCountdownZone(props: { endTime: number }): JSX.Element {
	return (
		<PreorderCountdownZoneContainer>
			<TitleContainer>
				<Typography variant={'h5'}>Pre-oder end in :</Typography>
			</TitleContainer>
			<PreorderCountdown endTime={props.endTime} />
		</PreorderCountdownZoneContainer>
	);
}

export default PreorderCountdownZone;
