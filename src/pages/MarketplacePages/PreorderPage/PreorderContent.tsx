import styled from 'styled-components';
import { majorScalePx } from 'ui-kit/utils';
import LeftSidebar from './LeftSidebar';
import { PreorderContentProps } from './types';
import PreorderDashboard from './PreorderDashboard';

const PreorderContentContainer = styled.div`
	display: flex;
	min-height: calc(100vh - ${majorScalePx(36)}); // UserMenu + SubNav
	width: 100%;
`;

function PreorderContent(props: PreorderContentProps): JSX.Element {
	return (
		<PreorderContentContainer>
			<LeftSidebar />
			<PreorderDashboard />
		</PreorderContentContainer>
	);
}

export default PreorderContent;
