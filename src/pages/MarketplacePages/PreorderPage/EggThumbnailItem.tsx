import React from 'react';
import history from 'routerHistory';
import styled from 'styled-components';
import { Box } from 'ui-kit/components/Box';
import getThemeValue from 'ui-kit/utils/getThemeValue';
import { majorScalePx } from 'ui-kit/utils/spacingRule/majorScalePx';
import {
	generateMarketPlaceItemUrl,
	MarketplacePageDirection,
} from '../MarketplacePageDirection';

const EggThumbnailItemContainer = styled(Box)`
	background: ${({ theme }) => getThemeValue('colors.primary')(theme)};
	height: ${majorScalePx(128)};
	&:hover {
		cursor: pointer;
	}
`;

function EggThumbnailItem() {
	return (
		<EggThumbnailItemContainer
			onClick={() => {
				history.push(generateMarketPlaceItemUrl(2));
			}}
		/>
	);
}

export default EggThumbnailItem;
