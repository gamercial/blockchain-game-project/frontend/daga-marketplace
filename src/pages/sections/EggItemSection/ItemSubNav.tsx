import React from 'react';
import { AutoRow } from 'components/Layout/Row';
import SectionLink from 'components/SectionLink';
import { majorScalePx } from 'ui-kit/utils/spacingRule/majorScalePx';
import { getBackLinkInfo } from './getBackLinkInfo';
import { useLocation } from 'react-router';

export type ItemSubNavProps = {
	itemPath: string;
	rootPath: string;
};

function ItemSubNav(props: ItemSubNavProps) {
	const location = useLocation();
	const backLink = getBackLinkInfo(
		location.pathname,
		props.itemPath,
		props.rootPath
	);
	console.log(`KDebug 🚩 ItemSubNav ~ ItemSubNav`, location);
	if (backLink == null) {
		return null;
	}
	return (
		<AutoRow
			marginBottom={`${majorScalePx(8)} !important`}
			gap={majorScalePx(1)}
		>
			<SectionLink
				noHighlight
				key={backLink.name}
				href={backLink.href}
				displayName={backLink.name}
				variant={'h5'}
			/>
		</AutoRow>
	);
}

export default ItemSubNav;
