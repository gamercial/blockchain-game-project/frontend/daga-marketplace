export type EggItemContentProps = {
	eggData: EggItemInfoProps;
};

export type EggItemOverviewProps = {};

export type EggItemDetailsProps = {
	item: EggItemInfoProps;
};

export type EggItemInfoProps = {
	id: string;
	name: string;
	price: number;
};

export type GroupBuyProps = {
	onClickBuy?(value: number): void;
};

export type EggClass = {
	id: number | string;
	name: string;
};

export type EggItemStatsProps = {};
