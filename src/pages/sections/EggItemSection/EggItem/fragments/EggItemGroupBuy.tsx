import React from 'react';
import styled from 'styled-components';
import { composedStyles, ComposedProps } from 'components/styles';
import Slider from 'ui-kit/components/Slider';
import { Button } from 'ui-kit/components/Button';
import { majorScale } from 'ui-kit/utils/spacingRule/majorScale';
import { majorScalePx } from 'ui-kit/utils';
import { safeRun } from 'utils/safeRun';
import { GroupBuyProps } from '../types';

const GroupBuyContainer = styled.div<ComposedProps>`
	${composedStyles}
`;

const QuantitySliderContainer = styled.div`
	margin-bottom: ${majorScalePx(10)};
`;

function EggItemGroupBuy(props: GroupBuyProps) {
	const { onClickBuy: onClickBuyProps } = props;

	const countOfEggs = React.useRef(1);

	const onChangeQty = (prev: number, value: number): void => {
		countOfEggs.current = value;
	};

	const onClickBuy = (): void => {
		safeRun(onClickBuyProps)(countOfEggs.current);
	};

	return (
		<GroupBuyContainer marginBottom={majorScale(10)}>
			<QuantitySliderContainer>
				<Slider />
			</QuantitySliderContainer>
			<Button onClick={onClickBuy} width={'100%'}>
				Buy now
			</Button>
		</GroupBuyContainer>
	);
}

export default EggItemGroupBuy;
