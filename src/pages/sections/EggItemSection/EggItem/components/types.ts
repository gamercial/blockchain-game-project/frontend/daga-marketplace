import { ReactNode } from 'react';
import { EggClass } from '../types';

export type EggItemTitleProps = {
	id?: string;
	itemsLeft?: string;
};

export type EggItemNameProps = {
	name: string;
	price: number;
};

export type EggStatsClassProps = {
	classInfo: EggClass;
	classIcon: ReactNode;
};

export type EggStatsProgressionProps = {};
