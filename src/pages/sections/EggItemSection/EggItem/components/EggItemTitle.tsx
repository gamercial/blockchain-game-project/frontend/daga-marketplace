import styled from 'styled-components';
import { Typography } from 'ui-kit/components/Typography';
import { majorScalePx } from 'ui-kit/utils';
import { EggItemTitleProps } from './types';

/**
 * Egg Item's id renderer
 */
const EggItemTitleContainer = styled.div`
	margin-bottom: ${majorScalePx(2)};
`;

export default function EggItemTitle(props: EggItemTitleProps) {
	return (
		<EggItemTitleContainer>
			<Typography variant={'h5'}>{props.id}</Typography>
		</EggItemTitleContainer>
	);
}
