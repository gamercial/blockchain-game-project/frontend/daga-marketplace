import styled from 'styled-components';
import { Flex } from 'ui-kit/components/Box';
import { Typography } from 'ui-kit/components/Typography';
import { majorScalePx } from 'ui-kit/utils';
import { EggStatsProgressionProps } from './types';
import { SampleEgg } from 'ui-kit/components/Svg';

const EggStatsProgressionContainer = styled.div``;

const ProgressionTitleContainer = styled.div`
	margin-bottom: ${majorScalePx(3)};
`;

const ProgressionInfoContainer = styled(Flex)``;

function EggStatsProgression(props: EggStatsProgressionProps): JSX.Element {
	return (
		<EggStatsProgressionContainer>
			<ProgressionTitleContainer>
				<Typography variant={'h5'}>Progression</Typography>
			</ProgressionTitleContainer>
			<ProgressionInfoContainer>
				<SampleEgg height={160} width={123} />
			</ProgressionInfoContainer>
		</EggStatsProgressionContainer>
	);
}

export default EggStatsProgression;
