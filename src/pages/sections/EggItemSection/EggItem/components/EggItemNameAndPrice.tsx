import styled from 'styled-components';
import { Typography } from 'ui-kit/components/Typography';
import { EggItemNameProps } from './types';
import SolanaPriceTag from 'components/SolanaPriceTag';

/**
 * Egg Item's name renderer
 */
const EggItemNameAndPriceContainer = styled.div`
	display: flex;
	justify-content: space-between;
`;

const EggItemPriceContainer = styled.div`
	align-items: center;
	display: flex;
`;

export default function EggItemNameAndPrice(props: EggItemNameProps) {
	return (
		<EggItemNameAndPriceContainer>
			<Typography variant={'h2'}>{props.name}</Typography>
			<EggItemPriceContainer>
				<SolanaPriceTag solana={0.244} usd={12} />
			</EggItemPriceContainer>
		</EggItemNameAndPriceContainer>
	);
}
