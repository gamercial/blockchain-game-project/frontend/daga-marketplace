import styled from 'styled-components';
import { useWeb3React } from '@web3-react/core';
import { useCallWithGasPrice } from 'hooks/useCallWithGasPrice';
import { useDagaEggContract } from 'hooks/useContract';
import { useAuth } from 'hooks/useAuth';
import { useWalletModal } from 'ui-kit/widgets/WalletModal';
import EggItemInfo from './fragments/EggItemInfo';
import EggItemGroupBuy from './fragments/EggItemGroupBuy';
import EggItemStats from './fragments/EggItemStats';
import type { EggItemDetailsProps } from './types';
import { parseEther } from '@ethersproject/units';
import { usePurchaseSuccessfulModal } from 'components/Modal';
import { useHistory } from 'react-router';

const EggItemDetailsContainer = styled.div`
	flex: 1;
`;

function EggItemDetails(props: EggItemDetailsProps) {
	const { item } = props;

	const { account } = useWeb3React();
	const { callWithGasPrice } = useCallWithGasPrice();
	const dagaEggContract = useDagaEggContract(true);
	const { login } = useAuth();
	const { onPresentConnectModal } = useWalletModal(login);
	const history = useHistory();

	const onGoToActivity = (): void => {
		onDismissPurchaseModal();
		history.push('/dashboard');
	};

	const [onPresentPurchaseModal, onDismissPurchaseModal] =
		usePurchaseSuccessfulModal({
			onGoToActivity,
			purchasedProductName: item.name,
		});

	const onClickBuy = (countOfEggs: number): void => {
		if (!dagaEggContract) {
			return;
		}
		if (!account) {
			return onPresentConnectModal();
		}

		const txReceipt = callWithGasPrice(
			dagaEggContract,
			'mintEgg',
			[item.name, item.id],
			{
				value: parseEther(item.price.toString()),
			}
		);

		txReceipt
			.then((res) => {
				console.log(`KDebug 🚩 EggItemDetails ~ res`, res);
				onPresentPurchaseModal();
			})
			.catch((err) => {
				console.log(`KDebug 🚩 EggItemDetails ~ err`, err);
			});
		console.log(
			'🚀 ~ file: GroupBuy.tsx ~ line 32 ~ onClickBuy ~ txReceipt',
			txReceipt
		);
	};

	return (
		<EggItemDetailsContainer>
			<EggItemInfo id={item.id} name={item.name} price={item.price} />
			<EggItemGroupBuy onClickBuy={onClickBuy} />
			<EggItemStats />
		</EggItemDetailsContainer>
	);
}

export default EggItemDetails;
