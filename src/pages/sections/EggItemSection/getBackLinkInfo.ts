import { matchPath } from 'react-router';

type BackLinkInfoResult = {
	name: string;
	href: string;
};

export function getBackLinkInfo(
	pathname: string,
	itemPath: string,
	rootPath: string
): BackLinkInfoResult | null {
	const matchMarketplaceItem = matchPath(pathname, {
		path: itemPath,
		exact: true,
		strict: false,
	});

	if (!isValidMatchItem(matchMarketplaceItem)) {
		return null;
	}

	return {
		name: '< Back',
		href: rootPath,
	};
}

function isValidMatchItem(matchItem: any): boolean {
	return !!matchItem;
}
