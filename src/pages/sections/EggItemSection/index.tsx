import React from 'react';
import styled from 'styled-components';
import { majorScalePx } from 'ui-kit/utils';
import ItemSubNav from './ItemSubNav';
import EggItemContent from './EggItem';

const EggItemSectionContainer = styled.div`
	padding-top: ${majorScalePx(10)};
	padding-left: ${majorScalePx(39)};
	padding-right: ${majorScalePx(39)};
`;

const sampleItem = {
	id: 'E01',
	name: 'Gold',
	price: 0.03,
};

export type EggItemSectionProps = {
	itemPath: string;
	rootPath: string;
};

function EggItemSection(props: EggItemSectionProps): JSX.Element {
	return (
		<EggItemSectionContainer>
			<ItemSubNav itemPath={props.itemPath} rootPath={props.rootPath} />
			<EggItemContent eggData={sampleItem} />
		</EggItemSectionContainer>
	);
}

export default EggItemSection;
