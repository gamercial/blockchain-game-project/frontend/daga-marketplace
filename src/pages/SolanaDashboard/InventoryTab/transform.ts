import { ArrayOwnedEggProperties, ObjectOwnedEggProperties } from './types';

export function transformArrayEggToObjectEgg(
	arrayEgg: ArrayOwnedEggProperties
): ObjectOwnedEggProperties {
	const [id, eggClass] = arrayEgg;
	return {
		id,
		eggClass,
	};
}
