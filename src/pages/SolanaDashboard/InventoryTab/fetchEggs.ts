import { Contract } from '@ethersproject/contracts';
import { transformArrayEggToObjectEgg } from './transform';
import { ArrayOwnedEggProperties, ObjectOwnedEggProperties } from './types';

export async function fetchEggs(
	dagaEggContract: Contract,
	account: string
): Promise<ObjectOwnedEggProperties[]> {
	const tokenIds: {
		_hex: string;
	}[] = await dagaEggContract.getOwnedTokenIds();

	const fetchedOwnedEggs: ObjectOwnedEggProperties[] = [];
	for (let i = 0; i < tokenIds.length; i++) {
		const detail: ArrayOwnedEggProperties =
			await dagaEggContract.getTokenProperty(tokenIds[i]._hex);
		fetchedOwnedEggs.push(transformArrayEggToObjectEgg(detail));
	}
	return fetchedOwnedEggs;
}
