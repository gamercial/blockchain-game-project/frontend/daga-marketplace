import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { useWeb3React } from '@web3-react/core';
import { useDagaEggContract } from 'hooks/useContract';
import { Box } from 'ui-kit/components/Box';
import { majorScalePx } from 'ui-kit/utils/spacingRule/majorScalePx';
import { EggData } from 'ui-kit/components/EggCard';
import { Dashboard } from 'ui-kit/components/Dashboard';
import EggItemSection from 'pages/sections/EggItemSection';
import { fetchEggs } from './fetchEggs';
import { InventoryTabProps, ObjectOwnedEggProperties } from './types';
import InventoryEggCard from './components/InventoryEggCard';
import { getTokensFromTracker } from '../getTokensFromTracker';
import { useConnection } from 'contexts/solana-connection';
import { useWallet } from '@solana/wallet-adapter-react';
import { Solana } from 'utils/pageDirectionConstant';
import { LayoutCenter } from 'ui-kit/utils';
import { getPublicImageResource } from 'utils/getResource';

const Dump = (): null => null;

function InventoryEggItemContent(props: InventoryTabProps): JSX.Element {
	if (props.pathSegment !== 'egg-item') return <Dump />;
	return (
		<EggItemSection
			itemPath={Solana.Dashboard.Item}
			rootPath={Solana.Dashboard.Root}
		/>
	);
}

const EmptyContent = styled.div`
	${LayoutCenter};
	height: 100%;
`;

const EmptyImageIllustration = styled.div`
	background-image: -webkit-image-set(
		url(${getPublicImageResource('inventory-empty-illustration@1x.png')}) 1x,
		url(${getPublicImageResource('inventory-empty-illustration@2x.png')}) 2x
	);
	height: 241px;
	width: 516px;
`;

function InventoryMainContent(
	props: InventoryTabProps & { eggs: Array<any> }
): JSX.Element {
	if (props.pathSegment) return <Dump />;
	if (!props.eggs.length)
		return (
			<EmptyContent>
				<EmptyImageIllustration />
			</EmptyContent>
		);
	const displayEggCards = props.eggs.map((egg) => getEggCardRenderer(egg));
	return <Dashboard items={displayEggCards} itemsPerPage={12} maxColumns={6} />;
}

const InventoryTabContainer = styled(Box)``;

function getEggCardRenderer(props: ObjectOwnedEggProperties): JSX.Element {
	const eggData: EggData = {
		id: props.id.toString(),
		name: props.eggClass,
	};
	return <InventoryEggCard eggData={eggData} />;
}

function InventoryTab(props: InventoryTabProps) {
	const connection = useConnection();
	const wallet = useWallet();

	const [ownedEggs, setOwnedEggs] = useState<ObjectOwnedEggProperties[]>([]);
	useEffect(() => {
		getTokensFromTracker({
			connection,
			wallet,
		}).then((a: any) => {
			console.log(`KDebug 🚩 index ~ `, a);
			setOwnedEggs(a);
		});
	}, [connection, wallet]);

	return (
		<InventoryTabContainer padding={majorScalePx(4)}>
			<InventoryMainContent {...props} eggs={ownedEggs} />
			<InventoryEggItemContent {...props} />
		</InventoryTabContainer>
	);
}

export default InventoryTab;
