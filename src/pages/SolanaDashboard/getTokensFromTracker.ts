import { WalletContextState } from '@solana/wallet-adapter-react';
import { Connection, PublicKey } from '@solana/web3.js';
import { getTrackerAccount } from 'pages/SolanaMarketplacePage/getTrackerAccount';
import { TrackerLayout } from 'solana-layout/layout';
import { ObjectOwnedEggProperties } from './InventoryTab/types';

export async function getTokensFromTracker({
	connection,
	wallet,
}: {
	connection: Connection;
	wallet: WalletContextState;
}): Promise<ObjectOwnedEggProperties[]> {
	if (!wallet.publicKey) {
		return [];
	}
	const accountInfo = await connection.getAccountInfo(
		await getTrackerAccount(wallet.publicKey)
	);
	if (accountInfo?.data) {
		const data = accountInfo.data.slice(1, accountInfo.data.length); // Ignore first byte for Sol logic purpose
		const trackerLayout = TrackerLayout();
		const decodedData = trackerLayout.decode(data);
		const decodedPublicKey = new PublicKey(decodedData.slot_key).toBase58();
		console.log(
			'🚀 ~ file: egg.ts ~ line 229 ~ getTracker ~ decodedData',
			decodedData
		);
		const readableData = {
			slotKey: decodedPublicKey,
			tokens: decodedData.tokens
				.map((tokenBuffer: Buffer) => {
					console.log(
						'🚀 ~ file: getTokensFromTracker.ts ~ line 32 ~ tokenBuffer',
						tokenBuffer
					);
					return new PublicKey(tokenBuffer).toBase58();
				})
				.filter(
					(token: string) => token !== '11111111111111111111111111111111'
				),
			isInitialized: !!decodedData.isInitialized,
		};
		console.log('Data: ', readableData);
		return readableData.tokens.map((token: string) => {
			return {
				id: token,
				eggClass: 'Gold',
			};
		});
	}
	return [];
}
