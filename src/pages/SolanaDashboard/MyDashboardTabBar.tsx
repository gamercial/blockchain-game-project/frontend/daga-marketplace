import React, { useState } from 'react';
import styled from 'styled-components';
import { Typography } from 'ui-kit/components/Typography';
import Row, { AutoRow } from 'components/Layout/Row';
import { PaddingXResponsive } from 'components/utils/constant';
import { LayoutCenter, majorScalePx } from 'ui-kit/utils';
import { Tab } from './tabList';

const MyDashboardTabBarContainer = styled.div`
	background-color: ${({ theme }) => `${theme.colors.backgroundStandard}`};
	box-sizing: border-box;
	height: ${majorScalePx(18)};
	${PaddingXResponsive}
`;

const TabBarItemContainer = styled.div<{
	isActive?: boolean;
}>`
	${LayoutCenter};
	border-bottom: ${({ isActive, theme }) =>
		isActive ? `3px solid ${theme.colors.primaryStandard}` : 'none'};
	cursor: pointer;
	height: calc(100% - 3px);
	margin-right: ${majorScalePx(4)};
	padding-left: ${majorScalePx(3)};
	padding-right: ${majorScalePx(3)};
`;

export type MyDashboardTabBarProps = {
	tabList: Array<Tab>;
	onChange?(index: number): void;
};

function MyDashboardTabBar(props: MyDashboardTabBarProps) {
	const { onChange: onChangeProps, tabList = [] } = props;
	const [activeTab, setActiveTab] = useState(0);

	const onClickTab = (index: number): void => {
		setActiveTab(index);
		if (onChangeProps) {
			onChangeProps(index);
		}
	};

	return (
		<MyDashboardTabBarContainer>
			<Row height={'100%'}>
				<AutoRow gap={majorScalePx(1)} height={'100%'}>
					{tabList.map((tab: Tab, index: number) => {
						return (
							<TabBarItemContainer
								key={tab.name}
								isActive={index === activeTab}
								onClick={() => onClickTab(index)}
							>
								<Typography
									variant={'h6'}
									color={index === activeTab ? 'textTitle' : 'textHolder'}
								>
									{tab.name}
								</Typography>
							</TabBarItemContainer>
						);
					})}
				</AutoRow>
			</Row>
		</MyDashboardTabBarContainer>
	);
}

export default MyDashboardTabBar;
