import { lazy } from 'react';

export type Tab = {
	name: string;
	content: any;
	props?: any;
	key: string;
};

const InventoryTab: Tab = {
	name: 'Inventory',
	key: 'Inventory',
	content: lazy(() => import('./InventoryTab')),
};

export const TabList = [InventoryTab];
