import { WalletContextState } from '@solana/wallet-adapter-react';
// @ts-ignore
import * as BufferLayout from 'buffer-layout';
import {
	Connection,
	PublicKey,
	SystemProgram,
	Transaction,
	TransactionInstruction,
} from '@solana/web3.js';
import { devnetProgramId } from 'configs/solanaProgramId';
import { TrackerLayout } from 'solana-layout/layout';

export async function createTrackerAccount({
	connection,
	payerPublicKey,
	wallet,
}: {
	connection: Connection;
	payerPublicKey: PublicKey;
	wallet: WalletContextState;
}) {
	const DATA_SLOT_SEED = 'tracker_account';

	const newStorageSlotPubKey = await PublicKey.createWithSeed(
		payerPublicKey,
		DATA_SLOT_SEED,
		devnetProgramId
	);
	console.log('user tracker account', newStorageSlotPubKey.toBase58());

	/**
	 * The expected size of the tracker storage.
	 */

	const STORAGE_SIZE = TrackerLayout().span + 1;
	const lamports = await connection.getMinimumBalanceForRentExemption(
		STORAGE_SIZE
	);

	const instructionToCreateAccount = new Transaction().add(
		SystemProgram.createAccountWithSeed({
			fromPubkey: payerPublicKey,
			basePubkey: payerPublicKey,
			seed: DATA_SLOT_SEED,
			newAccountPubkey: newStorageSlotPubKey,
			lamports,
			space: STORAGE_SIZE,
			programId: devnetProgramId,
		})
	);

	const dataLayout = BufferLayout.struct([BufferLayout.u8('instruction')]);

	const dto = {
		instruction: 1, // instruction
	};
	let data = Buffer.alloc(1024);
	const encodeLength = dataLayout.encode(dto, data);
	data = data.slice(0, encodeLength);
	console.log(
		'🚀 ~ file: egg.ts ~ line 120 ~ createTrackerAccount ~ data',
		data
	);

	const transaction = new Transaction();

	const instructionToCreateTrackerAccount = new TransactionInstruction({
		keys: [{ pubkey: newStorageSlotPubKey, isSigner: false, isWritable: true }],
		programId: devnetProgramId,
		data,
	});

	const block = await connection.getRecentBlockhash('singleGossip');
	transaction.recentBlockhash = block.blockhash;
	transaction.feePayer = payerPublicKey;
	transaction.add(instructionToCreateAccount);
	transaction.add(instructionToCreateTrackerAccount);

	return wallet.sendTransaction(transaction, connection);
}
