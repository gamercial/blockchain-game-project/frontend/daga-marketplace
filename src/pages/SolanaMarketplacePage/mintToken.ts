import { WalletContextState } from '@solana/wallet-adapter-react';
import {
	PublicKey,
	Connection,
	SystemProgram,
	Transaction,
	TransactionInstruction,
} from '@solana/web3.js';

// @ts-ignore
import * as BufferLayout from 'buffer-layout';
import { devnetProgramId } from 'configs/solanaProgramId';
import { EggMetadataLayout } from 'solana-layout/layout';
import { makeDString } from 'utils/makeDString';
import { getTrackerAccount } from './getTrackerAccount';

export async function mintToken({
	connection,
	payerPublicKey,
	wallet,
	description,
}: {
	connection: Connection;
	payerPublicKey: PublicKey;
	wallet: WalletContextState;
	description: string;
}): Promise<string> {
	const DATA_SLOT_SEED = Date.now().toString();
	const newDataSlotPubKey = await PublicKey.createWithSeed(
		payerPublicKey,
		DATA_SLOT_SEED,
		devnetProgramId
	);
	console.log(
		'🚀 ~ file: nft_solana.ts ~ line 102 ~ mint ~ newDataSlotPubKey',
		newDataSlotPubKey.toBase58()
	);

	/**
	 * The expected size of tinstructionhe NFT.
	 */
	const NFT_SIZE = 57;
	if (!wallet.signAllTransactions) {
		console.log('no');
		return 'failed';
	}
	const lamports = await connection.getMinimumBalanceForRentExemption(NFT_SIZE);
	console.log(
		'🚀 ~ file: nft_solana.ts ~ line 113 ~ mint ~ lamports',
		lamports
	);

	const transactionCreateDataSlot = new Transaction().add(
		SystemProgram.createAccountWithSeed({
			fromPubkey: payerPublicKey,
			basePubkey: payerPublicKey,
			seed: DATA_SLOT_SEED,
			newAccountPubkey: newDataSlotPubKey,
			lamports,
			space: NFT_SIZE,
			programId: devnetProgramId,
		})
	);

	const dataLayout = BufferLayout.struct([
		BufferLayout.u8('instruction'),
		EggMetadataLayout('metadata'),
	]);

	const dto = {
		instruction: 0, // instruction
		metadata: {
			ownerAddress: payerPublicKey.toBuffer(),
			description: Buffer.from(makeDString(description), 'utf-8'),
		},
	};
	let data = Buffer.alloc(1024);
	const encodeLength = dataLayout.encode(dto, data);
	data = data.slice(0, encodeLength);
	dataLayout.encode(dto, data);

	const instruction = new TransactionInstruction({
		keys: [
			{
				pubkey: await getTrackerAccount(payerPublicKey),
				isSigner: false,
				isWritable: true,
			},
			{ pubkey: newDataSlotPubKey, isSigner: false, isWritable: true },
		],
		programId: devnetProgramId,
		data,
	});
	const block = await connection.getRecentBlockhash('singleGossip');
	transactionCreateDataSlot.add(instruction);
	transactionCreateDataSlot.recentBlockhash = block.blockhash;
	transactionCreateDataSlot.feePayer = payerPublicKey;

	return wallet.sendTransaction(transactionCreateDataSlot, connection);
}
