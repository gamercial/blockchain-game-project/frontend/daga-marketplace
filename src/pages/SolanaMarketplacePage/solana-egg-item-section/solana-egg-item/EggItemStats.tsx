import styled from 'styled-components';
import { composedStyles, ComposedProps } from 'components/styles';
import { Typography } from 'ui-kit/components/Typography';
import { majorScalePx } from 'ui-kit/utils';
import type { EggItemStatsProps } from './types';
import EggStatsClass from './EggStatsClass';
import { IceClass } from 'ui-kit/components/Svg';
import EggStatsProgression from './EggStatsProgression';

const EggItemStatsContainer = styled.div<ComposedProps>`
	background: ${({ theme }) => theme.colors.backgroundStandard};
	border-radius: 24px;
	height: auto;
	padding: ${majorScalePx(6)} ${composedStyles};
`;

const StatsTitleContainer = styled.div`
	margin-bottom: ${majorScalePx(8)};
`;

const EggClassInfo = {
	id: 1,
	name: 'Ice',
};

function EggItemStats(props: EggItemStatsProps) {
	return (
		<EggItemStatsContainer>
			<StatsTitleContainer>
				<Typography variant={'h4'}>About</Typography>
			</StatsTitleContainer>
			<EggStatsClass classInfo={EggClassInfo} classIcon={<IceClass />} />
			<EggStatsProgression />
		</EggItemStatsContainer>
	);
}

export default EggItemStats;
