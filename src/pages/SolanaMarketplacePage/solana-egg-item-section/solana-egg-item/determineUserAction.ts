import { WalletContextState } from '@solana/wallet-adapter-react';
import { Connection } from '@solana/web3.js';
import { getTrackerAccount } from 'pages/SolanaMarketplacePage/getTrackerAccount';

export enum UserToAppState {
	NotConnected = 'not_connected',
	NeedCreatingTrackerAccount = 'need_creating_tracker',
	ReadyToBuy = 'ready_to_buy',
}

export async function determineAction({
	connection,
	wallet,
}: {
	connection: Connection;
	wallet: WalletContextState;
}): Promise<UserToAppState> {
	if (!wallet.publicKey) {
		return UserToAppState.NotConnected;
	}
	const userTrackerAccount = await getTrackerAccount(wallet.publicKey);
	const trackerData = await connection.getAccountInfo(userTrackerAccount);
	if (!trackerData) {
		return UserToAppState.NeedCreatingTrackerAccount;
	}
	return UserToAppState.ReadyToBuy;
}
