import styled from 'styled-components';
import { Image } from 'ui-kit/components/Image';
import { Typography } from 'ui-kit/components/Typography';
import { majorScalePx } from 'ui-kit/utils';
import { getPublicImageResource } from 'utils/getResource';
import { EggItemNameProps } from '../../../sections/EggItemSection/EggItem/components/types';

/**
 * Egg Item's name renderer
 */
const EggItemNameAndPriceContainer = styled.div`
	display: flex;
	justify-content: space-between;
`;

const EggItemPriceContainer = styled.div`
	align-items: center;
	display: flex;
`;

const PriceCoinContainer = styled.div`
	height: 20px;
	width: 20px;
`;

export default function EggItemNameAndPrice(props: EggItemNameProps) {
	return (
		<EggItemNameAndPriceContainer>
			<Typography variant={'h2'}>{props.name}</Typography>
			<EggItemPriceContainer>
				<Typography variant={'h4'} marginRight={majorScalePx(2)}>
					{props.price}
				</Typography>
				<PriceCoinContainer>
					<Image
						alt={'Coin'}
						height={20}
						src="https://cryptologos.cc/logos/solana-sol-logo.png?v=014"
						width={20}
					/>
				</PriceCoinContainer>
			</EggItemPriceContainer>
		</EggItemNameAndPriceContainer>
	);
}
