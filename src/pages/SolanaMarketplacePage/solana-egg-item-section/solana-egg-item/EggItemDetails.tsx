import styled from 'styled-components';
import { useWeb3React } from '@web3-react/core';
import { useCallWithGasPrice } from 'hooks/useCallWithGasPrice';
import { useDagaEggContract } from 'hooks/useContract';
import { useAuth } from 'hooks/useAuth';
import { useWalletModal } from 'ui-kit/widgets/WalletModal';
import EggItemInfo from './EggItemInfo';
import EggItemGroupBuy from './EggItemGroupBuy';
import EggItemStats from './EggItemStats';
import type { EggItemDetailsProps } from './types';
import { parseEther } from '@ethersproject/units';
import { usePurchaseSuccessfulModal } from 'components/Modal';
import { useHistory } from 'react-router';
import { TypographySingleLineText } from 'ui-kit/components/Typography/index.stories';
import { WalletMultiButton } from '@solana/wallet-adapter-ant-design';
import { Box } from 'ui-kit/components/Box';
import { majorScale } from 'ui-kit/utils';
import { useWallet } from '@solana/wallet-adapter-react';
import { mintToken } from 'pages/SolanaMarketplacePage/mintToken';
import { useConnection } from 'contexts/solana-connection';
import { determineAction, UserToAppState } from './determineUserAction';
import { useEffect, useState } from 'react';
import { Button } from 'ui-kit/components/Button';
import { createTrackerAccount } from 'pages/SolanaMarketplacePage/createTrackerAccount';

const EggItemDetailsContainer = styled.div`
	flex: 1;
`;

function EggItemDetails(props: EggItemDetailsProps) {
	const { item } = props;
	const wallet = useWallet();
	const connection = useConnection();
	const [actionState, setActionState] = useState<UserToAppState>(
		UserToAppState.NotConnected
	);
	const { connected, adapter } = wallet;
	const account = adapter?.publicKey?.toBase58();
	const { login } = useAuth();
	const { onPresentConnectModal } = useWalletModal(login);
	const history = useHistory();

	useEffect(() => {
		determineAction({
			connection,
			wallet,
		}).then(setActionState);
	}, [connection, wallet]);

	const onGoToActivity = (): void => {
		onDismissPurchaseModal();
		history.push('/solana/dashboard');
	};

	const [onPresentPurchaseModal, onDismissPurchaseModal] =
		usePurchaseSuccessfulModal({
			onGoToActivity,
			purchasedProductName: item.name,
		});

	const onClickBuy = async (countOfEggs: number): Promise<void> => {
		if (!wallet.publicKey) {
			return onPresentConnectModal();
		}
		const tx = await mintToken({
			connection,
			payerPublicKey: wallet.publicKey,
			wallet,
			description: 'Gold Class Egg',
		});
		//  window.alert(`tx: ${tx}`);
		onPresentPurchaseModal();
		console.log(
			'🚀 ~ file: EggItemDetails.tsx ~ line 56 ~ onClickBuy ~ tx',
			tx
		);
	};

	return (
		<EggItemDetailsContainer>
			<EggItemInfo id={item.id} name={item.name} price={item.price} />
			{actionState === UserToAppState.ReadyToBuy && (
				<EggItemGroupBuy onClickBuy={onClickBuy} />
			)}
			{actionState === UserToAppState.NotConnected && (
				<Box marginBottom={majorScale(10)}>
					<WalletMultiButton />
				</Box>
			)}
			{actionState === UserToAppState.NeedCreatingTrackerAccount && (
				<Box marginBottom={majorScale(10)}>
					<Button
						onClick={() => {
							if (!wallet.publicKey) {
								return;
							}
							createTrackerAccount({
								wallet,
								connection,
								payerPublicKey: wallet.publicKey,
							}).then((rs) => {
								window.location.reload();
							});
						}}
						width={'100%'}
					>
						Create tracker account
					</Button>
				</Box>
			)}

			<EggItemStats />
		</EggItemDetailsContainer>
	);
}

export default EggItemDetails;
