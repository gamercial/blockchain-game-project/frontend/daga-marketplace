import React from 'react';
import styled from 'styled-components';
import EggItemOverview from './EggItemOverview';
import EggItemDetails from './EggItemDetails';
import { EggItemContentProps } from './types';

const EggItemContentContainer = styled.div`
	display: flex;
`;

function EggItemContent(props: EggItemContentProps) {
	const { eggData } = props;
	return (
		<EggItemContentContainer>
			<EggItemOverview />
			<EggItemDetails item={eggData} />
		</EggItemContentContainer>
	);
}

export default EggItemContent;
