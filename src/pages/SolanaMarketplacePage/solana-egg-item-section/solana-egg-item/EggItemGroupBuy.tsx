import React from 'react';
import styled from 'styled-components';
import { composedStyles, ComposedProps } from 'components/styles';
import { CounterButton } from 'ui-kit/components/CounterButton';
import { Button } from 'ui-kit/components/Button';
import { majorScale } from 'ui-kit/utils/spacingRule/majorScale';
import { majorScalePx } from 'ui-kit/utils';
import { safeRun } from 'utils/safeRun';
import { GroupBuyProps } from './types';

const GroupBuyContainer = styled.div<ComposedProps>`
	display: flex;
	${composedStyles}
`;

function EggItemGroupBuy(props: GroupBuyProps) {
	const { onClickBuy: onClickBuyProps } = props;
	const countOfEggs = React.useRef(1);

	const onClickBuy = (): void => {
		safeRun(onClickBuyProps)(countOfEggs.current);
	};

	return (
		<GroupBuyContainer marginBottom={majorScale(10)}>
			<Button onClick={onClickBuy} width={'100%'}>
				Buy now
			</Button>
		</GroupBuyContainer>
	);
}

export default EggItemGroupBuy;
