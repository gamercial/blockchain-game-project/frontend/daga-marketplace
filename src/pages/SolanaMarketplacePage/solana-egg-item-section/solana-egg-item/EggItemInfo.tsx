import styled from 'styled-components';
import { composedStyles, ComposedProps } from 'components/styles';
import { majorScale } from 'ui-kit/utils/spacingRule/majorScale';
import type { EggItemInfoProps } from './types';
import EggItemTitle from './EggItemTitle';
import EggItemNameAndPrice from './SolanaEggItemNameAndPrice';

const EggItemInfoContainer = styled.div<ComposedProps>`
	${composedStyles}
`;

function EggItemInfo(props: EggItemInfoProps) {
	const { id, name, price } = props;
	return (
		<EggItemInfoContainer marginBottom={majorScale(10)}>
			<EggItemTitle id={id} />
			<EggItemNameAndPrice name={name} price={price} />
		</EggItemInfoContainer>
	);
}

export default EggItemInfo;
