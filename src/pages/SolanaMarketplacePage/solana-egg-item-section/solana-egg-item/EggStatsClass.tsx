import React, { ReactElement } from 'react';
import styled from 'styled-components';
import { Typography } from 'ui-kit/components/Typography';
import { majorScalePx } from 'ui-kit/utils';
import { EggStatsClassProps } from './types';

const EggStatsClassContainer = styled.div`
	margin-bottom: ${majorScalePx(8)};
`;

const ClassTitleContainer = styled.div`
	margin-bottom: ${majorScalePx(3)};
`;

const ClassInfoContainer = styled.div`
	display: flex;
`;

const ClassIconContainer = styled.div`
	margin-right: ${majorScalePx(2)};
`;

const ClassNameContainer = styled.div``;

function EggStatsClass(props: EggStatsClassProps): JSX.Element {
	const { classInfo, classIcon } = props;
	return (
		<EggStatsClassContainer>
			<ClassTitleContainer>
				<Typography variant={'h5'}>Class</Typography>
			</ClassTitleContainer>
			<ClassInfoContainer>
				<ClassIconContainer>
					{React.cloneElement(classIcon as ReactElement<any>, { width: 24 })}
				</ClassIconContainer>
				<ClassNameContainer>
					<Typography variant={'s1'}>{classInfo.name}</Typography>
				</ClassNameContainer>
			</ClassInfoContainer>
		</EggStatsClassContainer>
	);
}

export default EggStatsClass;
