import { ReactNode } from 'react';

export type EggItemContentProps = {
	eggData: EggItemInfoProps;
};

export type EggItemOverviewProps = {};

export type EggItemDetailsProps = {
	item: EggItemInfoProps;
};

export type EggItemInfoProps = {
	id: string;
	name: string;
	price: number;
};

export type GroupBuyProps = {
	onClickBuy?(value: number): void;
};

export type EggClass = {
	id: number | string;
	name: string;
};

export type EggItemStatsProps = {};

export type EggItemTitleProps = {
	id?: string;
	itemsLeft?: string;
};

export type EggStatsClassProps = {
	classInfo: EggClass;
	classIcon: ReactNode;
};

export type EggItemNameProps = {
	name: string;
	price: number;
};

export type EggStatsProgressionProps = {};
