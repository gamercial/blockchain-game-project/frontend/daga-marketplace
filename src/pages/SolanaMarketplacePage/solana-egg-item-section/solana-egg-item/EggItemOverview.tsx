import styled from 'styled-components';
import { composedStyles, ComposedProps } from 'components/styles';
import { majorScale } from 'ui-kit/utils/spacingRule/majorScale';
import { ShadowSampleEgg } from 'ui-kit/components/Svg';
import type { EggItemOverviewProps } from './types';

const EggItemOverviewContainer = styled.div<ComposedProps>`
	background: ${({ theme }) => theme.colors.backgroundDarker};
	flex: 1;
	${composedStyles}
`;

function EggItemOverview(props: EggItemOverviewProps) {
	return (
		<EggItemOverviewContainer marginRight={majorScale(17)} height={[758]}>
			<ShadowSampleEgg width={'100%'} />
		</EggItemOverviewContainer>
	);
}

export default EggItemOverview;
