import { MarketplacePageDirection } from 'pages/MarketplacePages/MarketplacePageDirection';
import { matchPath } from 'react-router';

type BackLinkInfoResult = {
	name: string;
	href: string;
};

export function getBackLinkInfo(pathname: string): BackLinkInfoResult | null {
	const matchMarketplaceItem = matchPath(pathname, {
		path: MarketplacePageDirection.MarketplaceItem,
		exact: true,
		strict: false,
	});

	if (!isValidMatchItem(matchMarketplaceItem)) {
		return null;
	}

	return {
		name: '< Back',
		href: '/solana' + MarketplacePageDirection.Root,
	};
}

function isValidMatchItem(matchItem: any): boolean {
	return !!matchItem;
}
