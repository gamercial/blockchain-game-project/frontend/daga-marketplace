import React from 'react';
import { AutoRow } from 'components/Layout/Row';
import SectionLink from 'components/SectionLink';
import { majorScalePx } from 'ui-kit/utils/spacingRule/majorScalePx';
import { getBackLinkInfo } from './getBackLinkInfo';
import { useLocation } from 'react-router';

function ItemSubNav() {
	const location = useLocation();
	const backLink = getBackLinkInfo(location.pathname);
	if (backLink == null) {
		return null;
	}
	return (
		<AutoRow
			marginBottom={`${majorScalePx(8)} !important`}
			gap={majorScalePx(1)}
		>
			<SectionLink
				noHighlight
				key={backLink.name}
				href={backLink.href}
				displayName={backLink.name}
				variant={'h5'}
			/>
		</AutoRow>
	);
}

export default ItemSubNav;
