import React from 'react';
import styled from 'styled-components';
import { majorScalePx } from 'ui-kit/utils';
import ItemSubNav from './ItemSubNav';
import EggItemContent from './solana-egg-item';

const EggItemSectionContainer = styled.div`
	padding-top: ${majorScalePx(10)};
	padding-left: ${majorScalePx(39)};
	padding-right: ${majorScalePx(39)};
`;

const sampleItem = {
	id: 'E01',
	name: 'Gold',
	price: 0.5,
};

function EggItemSection() {
	return (
		<EggItemSectionContainer>
			<ItemSubNav />
			<EggItemContent eggData={sampleItem} />
		</EggItemSectionContainer>
	);
}

export default EggItemSection;
