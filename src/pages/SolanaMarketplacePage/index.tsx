// import React, { useState } from 'react';
// import {
// 	WalletDisconnectButton,
// 	WalletMultiButton,
// } from '@solana/wallet-adapter-ant-design';
// import { useWallet } from '@solana/wallet-adapter-react';

import Page from 'components/Layout/Page';
import MarketplaceSubNav from 'pages/MarketplacePages/MarketplaceSubNav';
import PreorderContent from 'pages/MarketplacePages/PreorderPage/PreorderContent';
import { MarketplacePageProps } from 'pages/MarketplacePages/PreorderPage/types';
import EggItemSection from './solana-egg-item-section';

// import { Typography } from 'ui-kit/components/Typography';
// import { useSolanaBalance } from 'hooks/useSolanaBalance';
// import { getTokenProps, TokenProps } from './getTokenProps';
// import { useConnection } from 'contexts/solana-connection';
// import { mintToken } from './mintToken';

function MarketplacePage(props: MarketplacePageProps) {
	return (
		<Page meta={{ title: 'Gallumon - Marketplace' }}>
			<MarketplaceSubNav />
			<PreorderContent />
		</Page>
	);
}

export default MarketplacePage;

// function SolanaMarketplacePage() {
// 	const wallet = useWallet();
// 	const { connected, adapter } = wallet;
// 	const [nftAccountDetails, setNftAccountDetails] = useState<TokenProps | null>(
// 		null
// 	);
// 	const connection = useConnection();
// 	const solBalance = useSolanaBalance();
// 	const nftAccountAddress = 'H7Ui9ety3ktXEpywmAS1283XjZGrJ9N8LTVVW7vfLFCx';

// 	return (
// 		<div>
// 			<Typography variant={'s2'}>Solana here i come!</Typography>
// 			{connected ? (
// 				<WalletDisconnectButton type="ghost" />
// 			) : (
// 				<WalletMultiButton type="primary" />
// 			)}

// 			<Typography variant={'s2'}>
// 				{adapter?.publicKey?.toBase58()}
// 				&nbsp;having {solBalance}
// 			</Typography>

// 			<button
// 				onClick={() => {
// 					getTokenProps(connection, nftAccountAddress).then(
// 						setNftAccountDetails
// 					);
// 				}}
// 			>
// 				see NFT details of {nftAccountAddress}
// 			</button>
// 			{!!nftAccountDetails && (
// 				<Typography variant={'s2'}>
// 					{JSON.stringify(nftAccountDetails)}
// 				</Typography>
// 			)}
// 			<button
// 				onClick={async () => {
// 					if (!wallet.publicKey) {
// 						return;
// 					}
// 					await mintToken({
// 						connection,
// 						wallet,
// 						payerPublicKey: wallet.publicKey,
// 					});
// 				}}
// 			>
// 				Mint a token
// 			</button>
// 		</div>
// 	);
// }

// export default SolanaMarketplacePage;
