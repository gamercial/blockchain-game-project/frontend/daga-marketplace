import { Connection, PublicKey } from '@solana/web3.js';
import { NftAccountLayout } from 'solana-layout/layout.js';

export interface TokenProps {
	ownerAddress: string;
	metadata: string;
}

export async function getTokenProps(
	connection: Connection,
	base58PubKey: string
): Promise<TokenProps | null> {
	const nftAccountInfo = await connection.getAccountInfo(
		new PublicKey(base58PubKey)
	);
	console.log(
		'🚀 ~ file: nft_solana.ts ~ line 162 ~ getTokenProps ~ nftAccountInfo',
		nftAccountInfo
	);
	if (nftAccountInfo?.data) {
		const tokenLayout = NftAccountLayout();
		const decodedData = tokenLayout.decode(nftAccountInfo.data);
		const decodedPublicKey = new PublicKey(decodedData.ownerAddress).toBase58();
		console.log(
			'🚀 ~ file: nft_solana.ts ~ line 167 ~ getTokenProps ~ decodedPublicKey',
			decodedPublicKey
		);
		console.log(
			'🚀 ~ file: nft_solana.ts ~ line 166 ~ getTokenProps ~ decodedData',
			decodedData
		);
		return {
			ownerAddress: decodedPublicKey,
			metadata: decodedData.metadata,
		};
	}
	return null;
}
