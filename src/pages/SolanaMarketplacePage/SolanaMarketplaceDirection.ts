export enum MarketplacePageDirection {
	Root = '/solana/marketplace',
	Preorder = '/marketplace/preorder',
	EggList = '/solana/marketplace/eggs',
	MarketplaceItem = '/marketplace/item/:id',
}
