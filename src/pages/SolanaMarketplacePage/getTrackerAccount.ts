import { PublicKey } from '@solana/web3.js';
import { devnetProgramId } from 'configs/solanaProgramId';

export async function getTrackerAccount(
	payerPublicKey: PublicKey
): Promise<PublicKey> {
	const DATA_SLOT_SEED = 'tracker_account';

	const newStorageSlotPubKey = await PublicKey.createWithSeed(
		payerPublicKey,
		DATA_SLOT_SEED,
		devnetProgramId
	);
	return newStorageSlotPubKey;
}
