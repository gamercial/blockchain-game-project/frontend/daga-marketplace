import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { useWeb3React } from '@web3-react/core';
import { useDagaEggContract } from 'hooks/useContract';
import { Box } from 'ui-kit/components/Box';
import { majorScalePx } from 'ui-kit/utils/spacingRule/majorScalePx';
import { EggData } from 'ui-kit/components/EggCard';
import { Dashboard } from 'ui-kit/components/Dashboard';
import EggItemSection from 'pages/sections/EggItemSection';
import { fetchEggs } from './fetchEggs';
import { InventoryTabProps, ObjectOwnedEggProperties } from './types';
import InventoryEggCard from './components/InventoryEggCard';
import { MyDashboardPageDirection } from '../MyDashboardPageDirection';

const Dump = (): null => null;

function InventoryEggItemContent(props: InventoryTabProps): JSX.Element {
	if (props.pathSegment !== 'egg-item') return <Dump />;
	return (
		<EggItemSection
			itemPath={MyDashboardPageDirection.DashboardItem}
			rootPath={MyDashboardPageDirection.Root}
		/>
	);
}

function InventoryMainContent(
	props: InventoryTabProps & { eggs: Array<any> }
): JSX.Element {
	if (props.pathSegment) return <Dump />;
	const displayEggCards = props.eggs.map((egg) => getEggCardRenderer(egg));
	return <Dashboard items={displayEggCards} itemsPerPage={12} maxColumns={6} />;
}

const InventoryTabContainer = styled(Box)``;

function getEggCardRenderer(props: ObjectOwnedEggProperties): JSX.Element {
	const eggData: EggData = {
		id: props.id.toString(),
		name: props.eggClass,
	};
	return <InventoryEggCard eggData={eggData} />;
}

function InventoryTab(props: InventoryTabProps) {
	const dagaEggContract = useDagaEggContract(true);
	const { account } = useWeb3React();
	const [ownedEggs, setOwnedEggs] = useState<ObjectOwnedEggProperties[]>([]);
	useEffect(() => {
		if (!account || !dagaEggContract) {
			return;
		}

		fetchEggs(dagaEggContract, account).then((a: any) => {
			console.log(`KDebug 🚩 index ~ `, a);
			setOwnedEggs(a);
		});
	}, [account, dagaEggContract]);

	return (
		<InventoryTabContainer padding={majorScalePx(4)}>
			<InventoryMainContent {...props} eggs={ownedEggs} />
			<InventoryEggItemContent {...props} />
		</InventoryTabContainer>
	);
}

export default InventoryTab;
