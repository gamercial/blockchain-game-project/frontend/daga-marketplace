import styled from 'styled-components';
import { useHistory } from 'react-router';
import { EggCardWithID } from 'ui-kit/components/EggCard';
import { SampleEgg } from 'ui-kit/components/Svg';
import { InventoryEggCardProps } from '../types';

const InventoryEggCardContainer = styled.div``;

function InventoryEggCard(props: InventoryEggCardProps): JSX.Element {
	const { eggData } = props;
	const history = useHistory();
	return (
		<InventoryEggCardContainer>
			<EggCardWithID
				eggData={eggData}
				eggShape={<SampleEgg height={104} width={136} />}
				onClick={(data) => history.push(`/dashboard/item/${data.id}`)}
			/>
		</InventoryEggCardContainer>
	);
}

export default InventoryEggCard;
