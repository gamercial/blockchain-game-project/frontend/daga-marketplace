import { BigNumber } from '@ethersproject/bignumber';
import { EggData } from 'ui-kit/components/EggCard'; // Should move EggCard to /components folder

export type ArrayOwnedEggProperties = [BigNumber, string, string];

export type ObjectOwnedEggProperties = {
	id: BigNumber;
	eggClass: string;
};

export type InventoryTabProps = {
	pathSegment?: string;
};

export type InventoryEggCardProps = {
	eggData: EggData;
};
