export enum MyDashboardPageDirection {
	Root = '/dashboard',
	DashboardItem = '/dashboard/item/:id',
}
