import { useState } from 'react';
import Page from 'components/Layout/Page';
import MyDashboardTabBar from './MyDashboardTabBar';
import { TabList, Tab } from './tabList';

function MyDashboard(props: any): JSX.Element {
	const [activeTab, setActiveTab] = useState(0);
	const onChangeTab = (index: number): void => {
		setActiveTab(index);
	};

	return (
		<Page>
			<MyDashboardTabBar tabList={TabList} onChange={onChangeTab} />
			{TabList.map((tab: Tab, index: number) => {
				if (index === activeTab) {
					return <tab.content {...props} {...tab.props} />;
				}
				return null;
			})}
		</Page>
	);
}

export default MyDashboard;
