import { lazy } from 'react';

export type Tab = {
	name: string;
	content: any;
	props?: any;
	key: string;
};

const InventoryTab: Tab = {
	name: 'Inventory',
	key: 'Inventory',
	content: lazy(() => import('./InventoryTab')),
};

const ActivityTab: Tab = {
	name: 'Activity',
	key: 'Activity',
	content: lazy(() => import('./ActivityTab')),
};

export const TabList = [InventoryTab, ActivityTab];
