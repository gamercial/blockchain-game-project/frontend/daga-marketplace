import { ActivityInDay } from './types';

export const Activities: ActivityInDay[] = [
	{
		date: 1633251664000,
		items: [
			{
				buyer: {
					id: 1,
					name: 'Khang',
					displayName: 'You',
				},
				product: {
					id: 1,
					name: 'Dragon Egg',
				},
				price: {
					amount: 0.1,
					currency: 'BNB',
				},
				seller: {
					id: 1,
					name: 'Khang',
					displayName: 'You',
				},
				tradingId: 1,
				timestamp: 1633251639000,
			},
			{
				buyer: {
					id: 1,
					name: 'Khang',
					displayName: 'You',
				},
				product: {
					id: 2,
					name: 'Dragon Egg 2',
				},
				price: {
					amount: 0.2,
					currency: 'BNB',
				},
				seller: {
					id: 1,
					name: 'Khang',
					displayName: 'You',
				},
				tradingId: 2,
				timestamp: 1633251664000,
			},
		],
	},
	{
		date: 1633338064000,
		items: [
			{
				buyer: {
					id: 1,
					name: 'Khang',
					displayName: 'You',
				},
				product: {
					id: 3,
					name: 'Dragon Egg 3',
				},
				price: {
					amount: 0.3,
					currency: 'BNB',
				},
				seller: {
					id: 1,
					name: 'Khang',
					displayName: 'You',
				},
				tradingId: 1,
				timestamp: 1633338779000,
			},
			{
				buyer: {
					id: 1,
					name: 'Khang',
					displayName: 'You',
				},
				product: {
					id: 4,
					name: 'Dragon Egg 4',
				},
				price: {
					amount: 0.4,
					currency: 'BNB',
				},
				seller: {
					id: 1,
					name: 'Khang',
					displayName: 'You',
				},
				tradingId: 2,
				timestamp: 1633339064000,
			},
		],
	},
];
