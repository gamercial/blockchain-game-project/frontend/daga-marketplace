import styled from 'styled-components';
import { majorScalePx } from 'ui-kit/utils';
import ActivitySection from './ActivitySection';
import { Activities } from './config';
import { ActivityInDay, ActivityItem, ActivityTabProps } from './types';

export function sortActivities(
	activities: Array<ActivityInDay>
): Array<ActivityInDay> {
	const clonedArray: Array<ActivityInDay> = activities.slice();
	let clonedItems: Array<ActivityItem> = [];
	for (let i = 0; i < clonedArray.length; i++) {
		clonedItems = clonedArray[i].items;
		clonedArray[i].items = clonedItems.sort(
			(a, b) => b.timestamp - a.timestamp
		);
	}
	return clonedArray.sort((a, b) => b.date - a.date);
}

const ActivityTabContainer = styled.div`
	padding-top: ${majorScalePx(16)};
	padding-left: ${majorScalePx(39)};
	padding-right: ${majorScalePx(39)};
`;

function ActivityTab(props: ActivityTabProps) {
	const { activities } = props;
	return (
		<ActivityTabContainer>
			{activities.map((activity, index) => {
				return (
					<ActivitySection
						activity={activity}
						key={activity.date}
						marginBottom={
							index !== activities.length - 1 ? majorScalePx(12) : 0
						}
					/>
				);
			})}
		</ActivityTabContainer>
	);
}

ActivityTab.defaultProps = {
	activities: sortActivities(Activities),
};
export default ActivityTab;
