import { SpaceProps } from 'styled-system';

export type UserMiniData = {
	id: number | string;
	name: string;
	displayName: string;
};

export type ActivityItem = {
	buyer: UserMiniData;
	product: {
		id: number | string;
		name: string;
	};
	price: {
		amount: number;
		currency: string;
	};
	seller: UserMiniData;
	tradingId: number | string;
	timestamp: number;
};

export type ActivityInDay = {
	date: number;
	items: Array<ActivityItem>;
};

export type ActivityTabProps = {
	activities: Array<ActivityInDay>;
};

export interface ActivitySectionProps extends SpaceProps {
	activity: ActivityInDay;
}

export interface ActivityRowItemProps extends SpaceProps {
	data: {
		actionName: string;
		productName: string;
		price: {
			amount: number | string;
			currency: string;
		};
		subjectName: string;
		timestamp: number | string;
		tradingId: number | string;
	};
}
