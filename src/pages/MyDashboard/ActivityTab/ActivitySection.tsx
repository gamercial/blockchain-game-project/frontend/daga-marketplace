import styled from 'styled-components';
import { space } from 'styled-system';
import { pad } from 'utils/timeFormatter';
import { Typography } from 'ui-kit/components/Typography';
import { majorScalePx } from 'ui-kit/utils';
import ActivityRowItem from './ActivityRowItem';
import { ActivityItem, ActivitySectionProps } from './types';

const ActivitySectionContainer = styled.div`
	${space}
`;

const DateTitleContainer = styled.div`
	margin-bottom: ${majorScalePx(8)};
`;

const ActivityContentContainer = styled.div``;

function getActivityDateTimeRenderer(timestamp: number): string {
	const time = new Date(timestamp);
	return (
		pad(time.getDate(), 2) +
		'/' +
		pad(time.getMonth() + 1, 2) +
		'/' +
		pad(time.getFullYear(), 2)
	);
}

function ActivitySection(props: ActivitySectionProps): JSX.Element {
	const { activity } = props;
	const items = activity.items;
	return (
		<ActivitySectionContainer {...props}>
			<DateTitleContainer>
				<Typography variant={'h3'}>
					{getActivityDateTimeRenderer(activity.date)}
				</Typography>
			</DateTitleContainer>
			<ActivityContentContainer>
				{items.map((item: ActivityItem, index: number) => {
					const data = {
						actionName: 'bought',
						productName: item.product.name,
						price: item.price,
						subjectName: 'You',
						timestamp: item.timestamp,
						tradingId: item.tradingId,
					};
					return (
						<ActivityRowItem
							key={data.tradingId}
							data={data}
							marginBottom={index !== items.length - 1 ? majorScalePx(8) : 0}
						/>
					);
				})}
			</ActivityContentContainer>
		</ActivitySectionContainer>
	);
}

export default ActivitySection;
