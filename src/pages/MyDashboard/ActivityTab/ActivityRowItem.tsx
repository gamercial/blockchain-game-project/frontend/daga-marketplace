import styled from 'styled-components';
import { space } from 'styled-system';
import { Typography } from 'ui-kit/components/Typography';
import { majorScalePx } from 'ui-kit/utils';
import { pad } from 'utils/timeFormatter';
import { ActivityRowItemProps } from './types';

const ActivityRowItemContainer = styled.div`
	display: flex;
	width: 100%;
	${space}
`;

const TimeRegionContainer = styled.div`
	width: ${majorScalePx(30)};
`;

const InfoContainer = styled.div``;

const MainInfoContainer = styled.div`
	display: flex;
	margin-bottom: ${majorScalePx(2)};
`;

const TradingInfoContainer = styled.div``;

function getActivityTimeRenderer(timestamp: number): string {
	const time = new Date(timestamp);
	return (
		pad(time.getHours(), 2) +
		':' +
		pad(time.getMinutes(), 2) +
		':' +
		pad(time.getSeconds(), 2)
	);
}

function ActivityRowItem(props: ActivityRowItemProps): JSX.Element {
	const { data } = props;
	return (
		<ActivityRowItemContainer {...props}>
			<TimeRegionContainer>
				<Typography variant={'s1'} color={'textHolder'}>
					{getActivityTimeRenderer(data.timestamp as number)}
				</Typography>
			</TimeRegionContainer>
			<InfoContainer>
				<MainInfoContainer>
					<Typography variant={'s1'}>
						{data.subjectName} {data.actionName}
					</Typography>
					<Typography variant={'s1'} color={'primaryStandard'}>
						&nbsp;{data.productName}&nbsp;
					</Typography>
					<Typography variant={'s1'}>
						for {data.price.amount} {data.price.currency}
					</Typography>
				</MainInfoContainer>
				<TradingInfoContainer>
					<Typography variant={'s1'}>Trading ID : #{data.tradingId}</Typography>
				</TradingInfoContainer>
			</InfoContainer>
		</ActivityRowItemContainer>
	);
}

export default ActivityRowItem;
