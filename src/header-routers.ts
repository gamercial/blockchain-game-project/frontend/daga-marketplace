import { lazy } from 'react';
import { RouterType } from 'routers';
import { Solana } from 'utils/pageDirectionConstant';

const SolanaHeaderRouter: RouterType = {
	path: Solana.Root,
	exact: false,
	children: lazy(() => import('./components/SolanaHeader')),
};
const BscHeaderRouter: RouterType = {
	path: '/',
	exact: false,
	children: lazy(() => import('./components/UserMenu')),
};

const HeaderRouters = [SolanaHeaderRouter, BscHeaderRouter];

export default HeaderRouters;
