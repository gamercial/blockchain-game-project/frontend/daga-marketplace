export const BSC = {
	Marketplace: {
		Root: '/marketplace',
		Preorder: '/marketplace/preorder',
		Item: '/marketplace/item/:id',
	},

	Dashboard: {
		Root: '/dashboard',
		Preorder: '/marketplace/preorder',
		Item: '/marketplace/item/:id',
	},
};

const SolanaRoot = '/solana';
export const Solana = {
	Root: SolanaRoot,
	Marketplace: {
		Root: SolanaRoot + '/marketplace',
		Preorder: SolanaRoot + '/marketplace/preorder',
		Item: SolanaRoot + '/marketplace/item/:id',
	},

	Dashboard: {
		Root: SolanaRoot + '/dashboard',
		Preorder: SolanaRoot + '/marketplace/preorder',
		Item: SolanaRoot + '/marketplace/item/:id',
	},
};
