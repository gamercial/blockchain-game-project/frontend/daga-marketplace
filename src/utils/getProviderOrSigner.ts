import { Web3Provider, JsonRpcSigner } from '@ethersproject/providers';
import { getSigner } from './getSigner';

// account is optional
export function getProviderOrSigner(
	library: Web3Provider,
	account?: string
): Web3Provider | JsonRpcSigner {
	return account ? getSigner(library, account) : library;
}
