import { Contract, ContractFunction, ethers } from 'ethers';

export interface DagaSystemInfoContract extends Contract {
	getSaleOpenTime: ContractFunction<ethers.BigNumber>;
}

export interface DagaEggContract extends Contract {
	mintEgg(): ContractFunction<void>;
}
