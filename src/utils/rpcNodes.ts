export const rpcNodes = [
	process.env.REACT_APP_RPC_NODE_1 as string,
	process.env.REACT_APP_RPC_NODE_2 as string,
	process.env.REACT_APP_RPC_NODE_3 as string,
];
