export function pad(num: number, length: number): string {
	let sNum: string = num.toString();
	while (sNum.length < length) sNum = '0' + sNum;
	return sNum;
}
