function getResource(path: string, fileName: string): string {
	return path + fileName;
}

const ImagePublicRootPath: string = '/static/images/';
export function getPublicImageResource(fileName: string): string {
	return getResource(process.env.PUBLIC_URL + ImagePublicRootPath, fileName);
}
