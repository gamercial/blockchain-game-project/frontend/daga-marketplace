import addresses from 'configs/constants/contractAddresses';
import { getAddress } from './getAddress';

export const getDagaSystemInfoAddress = () => {
	return getAddress(addresses.dagaSystemInfo);
};

export const getDagaEggAddress = () => {
	return getAddress(addresses.dagaEgg);
};
