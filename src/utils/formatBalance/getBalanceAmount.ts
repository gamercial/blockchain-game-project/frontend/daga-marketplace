import BigNumber from 'bignumber.js';
import { BIG_TEN } from 'utils/bigNumber';

export const getBalanceAmount = (amount: BigNumber, decimals = 18) => {
	return new BigNumber(amount).dividedBy(BIG_TEN.pow(decimals));
};
