import { getBalanceAmount } from './getBalanceAmount';
import BigNumber from 'bignumber.js';

export const getFullDisplayBalance = (
	balance: BigNumber,
	decimals = 18,
	displayDecimals?: number
) => {
	if (!displayDecimals) {
		return getBalanceAmount(balance, decimals);
	}
	return getBalanceAmount(balance, decimals).toFixed(displayDecimals);
};
