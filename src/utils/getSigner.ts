import { Web3Provider, JsonRpcSigner } from '@ethersproject/providers';

// account is not optional
export function getSigner(
	library: Web3Provider,
	account: string
): JsonRpcSigner {
	return library.getSigner(account).connectUnchecked();
}
