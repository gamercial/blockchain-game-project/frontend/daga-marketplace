export function makeDString(str: string): string {
	if (str.length > 32) {
		throw new Error('DString length exceed 32');
	}
	return (
		str +
		Array(32 - str.length + 1)
			.map(() => String.fromCharCode(32))
			.join(' ')
	);
}
