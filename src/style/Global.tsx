import { createGlobalStyle } from 'styled-components';
import { DagaTheme } from 'ui-kit/theme';
// eslint-disable-next-line import/no-unresolved

declare module 'styled-components' {
	/* eslint-disable @typescript-eslint/no-empty-interface */
	export interface DefaultTheme extends DagaTheme {}
}

const GlobalStyle = createGlobalStyle`
  * {
    font-family: 'DM Sans', sans-serif;
  }
  body {
    background-color: ${({ theme }) => theme.colors.backgroundDarker};

    img {
      height: auto;
      max-width: 100%;
    }

    ::-webkit-scrollbar {
      width: 8px;
    }

    ::-webkit-scrollbar-track {
      box-shadow: inset 0 0 6px ${({ theme }) => theme.colors.backgroundDarker};
    }

    ::-webkit-scrollbar-thumb {
      background-color: ${({ theme }) => theme.colors.backgroundLighter};
      border-radius: 8px;
    }
  }
`;

export default GlobalStyle;
