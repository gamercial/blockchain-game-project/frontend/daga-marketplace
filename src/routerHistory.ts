import { createBrowserHistory } from 'history';

//* explicitly create to be able to access from modal -> nice idea from brother pancake
const history = createBrowserHistory();

export default history;
