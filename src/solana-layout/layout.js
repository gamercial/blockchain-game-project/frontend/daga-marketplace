/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import * as BufferLayout from 'buffer-layout';

/**
 * Layout for a public key
 */
export const publicKey = (property = 'publicKey') => {
	return BufferLayout.blob(32, property);
};

export const staticString = (property = 'string') => {
	const rsl = BufferLayout.struct([BufferLayout.blob(24, 'chars')], property);
	const _decode = rsl.decode.bind(rsl);
	const _encode = rsl.encode.bind(rsl);

	rsl.decode = (buffer, offset) => {
		const data = _decode(buffer, offset);
		return data.chars.toString('utf8');
	};

	rsl.encode = (str, buffer, offset) => {
		const data = {
			chars: Buffer.from(str, 'utf8'),
		};
		return _encode(data, buffer, offset);
	};

	return rsl;
};

export const NftAccountLayout = () => {
	const layout = BufferLayout.struct([
		publicKey('ownerAddress'),
		staticString('metadata'),
		BufferLayout.u8('isInitialized'),
	]);
	return layout;
};

export const EggMetadataLayout = (property = 'string') => {
	return BufferLayout.struct(
		[
			BufferLayout.blob(32, 'ownerAddress'),
			BufferLayout.blob(32, 'description'),
		],
		property
	);
};

export const EggAttributeLayout = (property = 'string') => {
	return BufferLayout.struct([BufferLayout.blob(32, 'randSeed')], property);
};

export const EggLayout = (property = 'string') => {
	return BufferLayout.struct(
		[
			publicKey('slot_key'),
			EggMetadataLayout('metadata'),
			EggAttributeLayout('attribute'),
			BufferLayout.u8('isInitialized'),
		],
		property
	);
};

export const TrackerLayout = (property = 'tracker') => {
	return BufferLayout.struct(
		[
			publicKey('slot_key'),
			// 1 account can store 10 eggs for current techical limitation
			BufferLayout.seq(publicKey(), 10, 'tokens'),
			BufferLayout.u8('isInitialized'),
		],
		property
	);
};
