import React, { useMemo } from 'react';
import { ModalProvider } from 'ui-kit';
import {
	getLedgerWallet,
	getMathWallet,
	getPhantomWallet,
	getSolflareWallet,
	getSolletWallet,
	getSolongWallet,
	getTorusWallet,
} from '@solana/wallet-adapter-wallets';
import { WalletProvider as SolanaWalletProvider } from '@solana/wallet-adapter-react';
import { WalletModalProvider as SolanaWalletModalProvider } from '@solana/wallet-adapter-ant-design';
import { Web3ReactProvider } from '@web3-react/core';
import { ThemeProvider } from 'styled-components';
import { HelmetProvider } from 'react-helmet-async';
import { getLibrary } from 'web3/getLibrary';
import { dark } from 'ui-kit/theme';
import { SolanaConnectionProvider } from 'contexts/solana-connection';

interface IProps {
	children: React.ReactNode;
}

export function Providers(props: IProps) {
	const wallets = useMemo(
		() => [
			getPhantomWallet(),
			getSolflareWallet(),
			getTorusWallet({
				options: {
					// TODO: Get your own tor.us wallet client Id
					clientId:
						'BOM5Cl7PXgE9Ylq1Z1tqzhpydY0RVr8k90QQ85N7AKI5QGSrr9iDC-3rvmy0K_hF0JfpLMiXoDhta68JwcxS1LQ',
				},
			}),
			getLedgerWallet(),
			getSolongWallet(),
			getMathWallet(),
			getSolletWallet(),
		],
		[]
	);
	return (
		<SolanaConnectionProvider>
			<SolanaWalletProvider wallets={wallets} autoConnect>
				<SolanaWalletModalProvider>
					<Web3ReactProvider getLibrary={getLibrary}>
						<HelmetProvider>
							<ThemeProvider theme={dark}>
								<ModalProvider>{props.children}</ModalProvider>
							</ThemeProvider>
						</HelmetProvider>
					</Web3ReactProvider>
				</SolanaWalletModalProvider>
			</SolanaWalletProvider>
		</SolanaConnectionProvider>
	);
}
