import React from 'react';
import styled from 'styled-components';
import { Helmet } from 'react-helmet-async';
import { DEFAULT_META } from 'configs/constants/meta';
import Container from './Container';

const StyledPage = styled(Container)`
	// padding-top: 16px;
	// padding-bottom: 16px;
	//
	// ${({ theme }) => theme.mediaQueries.sm} {
	// 	padding-top: 24px;
	// 	padding-bottom: 24px;
	// }
	//
	// ${({ theme }) => theme.mediaQueries.lg} {
	// 	padding-top: 32px;
	// 	padding-bottom: 32px;
	// }
`;

const PageMeta = ({ title }: { title?: string }) => {
	const { title: defaultTitle, description, image } = { ...DEFAULT_META };
	const pageTitle = title || defaultTitle;

	return (
		<Helmet>
			<title>{pageTitle}</title>
			<meta property="og:title" content={title} />
			<meta property="og:description" content={description} />
			<meta property="og:image" content={image} />
		</Helmet>
	);
};

const Page: React.FC<
	React.HTMLAttributes<HTMLDivElement> & {
		meta?: {
			title: string;
		};
	}
> = ({ children, ...props }) => {
	return (
		<>
			<PageMeta {...props.meta} />
			<StyledPage {...props}>{children}</StyledPage>
		</>
	);
};

export default Page;
