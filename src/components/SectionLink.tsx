import React, { ReactElement, useContext } from 'react';
import { useLocation } from 'react-router';
import styled, { ThemeContext } from 'styled-components';
import { Typography } from 'ui-kit/components/Typography';
import { majorScale } from 'ui-kit/utils/spacingRule/majorScale';
import { majorScalePx } from 'ui-kit/utils/spacingRule/majorScalePx';
import { LayoutCenter } from 'ui-kit/utils';

const SectionLinkContainer = styled.a<{
	isActive: boolean;
}>`
	display: flex;
	margin: 0 ${majorScale(1)};
	text-decoration: none;
	.icon {
		margin-right: ${majorScalePx(2)};
	}
`;

const SectionLinkIconContainer = styled.div`
	${LayoutCenter}
`;

interface IProps {
	IconComponent?: ReactElement;
	noHighlight?: boolean;
	href: string;
	displayName: string;
	variant?: string;
}

function SectionLink(props: IProps) {
	const theme = useContext(ThemeContext);
	const location = useLocation();
	const { IconComponent, variant } = props;
	const isActive = !props.noHighlight && location.pathname.includes(props.href);
	return (
		<SectionLinkContainer href={props.href} isActive={isActive}>
			{!!props.IconComponent && (
				<SectionLinkIconContainer>
					{React.cloneElement(IconComponent as ReactElement<any>, {
						color: isActive ? theme.colors.primaryStandard : theme.colors.white,
						className: 'icon',
					})}
				</SectionLinkIconContainer>
			)}
			<Typography
				color={isActive ? 'primaryStandard' : 'white'}
				variant={variant ? variant : 's3'}
			>
				{props.displayName}
			</Typography>
		</SectionLinkContainer>
	);
}

export default SectionLink;
