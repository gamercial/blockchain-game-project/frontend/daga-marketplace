import styled from 'styled-components';
import { Typography } from 'ui-kit/components/Typography';
import { LayoutCenter, majorScalePx } from 'ui-kit/utils';
import { getPublicImageResource } from 'utils/getResource';

export type SolanaPriceProps = {
	solana: number;
	usd: number;
};

const SolanaPriceTagContainer = styled.div`
	${LayoutCenter};
`;

const SolanaPriceContainer = styled.div`
	${LayoutCenter};
`;

const SolanaIcon = styled.div`
	background-image: -webkit-image-set(
		url(${getPublicImageResource('solana@2x.png')}) 1x,
		url(${getPublicImageResource('solana@4x.png')}) 2x
	);
	background-size: cover;
	height: 16px;
	margin-left: ${majorScalePx(1)};
	width: 16px;
`;

const SeparatorContainer = styled.div`
	${LayoutCenter};
	height: 100%;
	margin-left: ${majorScalePx(4)};
	margin-right: ${majorScalePx(4)};
`;

const Separator = styled.div`
	background-color: ${({ theme }) => theme.colors.backgroundStroke};
	height: 12px;
	width: 1px;
`;

const USDPriceContainer = styled.div``;

function SolanaPriceTag(props: SolanaPriceProps): JSX.Element {
	return (
		<SolanaPriceTagContainer>
			<SolanaPriceContainer>
				<Typography variant={'h4'} color={'textTitle'}>
					{props.solana}
				</Typography>
				<SolanaIcon />
			</SolanaPriceContainer>
			<SeparatorContainer>
				<Separator />
			</SeparatorContainer>
			<USDPriceContainer>
				<Typography variant={'p1'} color={'textBody'}>
					${props.usd}
				</Typography>
			</USDPriceContainer>
		</SolanaPriceTagContainer>
	);
}

export default SolanaPriceTag;
