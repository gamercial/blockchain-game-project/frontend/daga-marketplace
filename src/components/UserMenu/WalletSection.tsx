import React from 'react';
import styled from 'styled-components';
import { getFullDisplayBalance } from 'utils/formatBalance/getFullDisplayBalance';
import { WalletConnectButton } from 'components/WalletConnectButton';
import { useGetBnbBalance } from 'hooks/useGetBnbBalance';
import { FetchStatus } from 'hooks/types/FetchStatus.model';
import { Typography } from 'ui-kit/components/Typography';
import { MetamaskIcon } from 'ui-kit/components/Icon';
import { Image } from 'ui-kit/components/Image';
import { LayoutCenter, majorScalePx } from 'ui-kit/utils';
import { getPublicImageResource } from '../../utils/getResource';

export const WalletSectionContainer = styled.div`
	display: flex;
`;

export const WalletInfoContainer = styled.div`
	border-right: 1px solid rgba(255, 255, 255, 0.07);
	display: flex;
	margin-right: ${majorScalePx(4)};
	padding-right: ${majorScalePx(2)};
`;

export const IconContainer = styled.div`
	${LayoutCenter};
	background-color: ${({ theme }) => theme.colors.backgroundLighter};
	border: 1px solid ${({ theme }) => theme.colors.backgroundStroke};
	border-radius: 12px;
	height: ${majorScalePx(12)};
	margin-right: ${majorScalePx(2)};
	width: ${majorScalePx(12)};
`;

export const UserInfoContainer = styled.div`
	width: ${majorScalePx(40)};
`;

export const TextOverflowBase = styled.div`
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
	width: 100%;
`;

export const UserInfoCoinContainer = styled(TextOverflowBase)`
	color: ${({ theme }) => theme.colors.textTitle};
`;

export const UserInfoWalletAddressContainer = styled.div`
	display: flex;
`;

export const UserInfoWalletAddressHeadContainer = styled(TextOverflowBase)`
	color: ${({ theme }) => theme.colors.textBody};
`;

export const UserInfoWalletAddressEndContainer = styled.div`
	color: ${({ theme }) => theme.colors.textBody};
`;

export const UserAvatarContainer = styled.div`
	height: ${majorScalePx(12)};
	width: ${majorScalePx(12)};
`;

interface IProps {
	account?: string | null;
}

function WalletSection(props: IProps) {
	const { balance, fetchStatus } = useGetBnbBalance();
	console.log(`KDebug 🚩 WalletSection ~ WalletSection`, props.account);
	if (!props.account) {
		return <WalletConnectButton />;
	}
	return (
		<WalletSectionContainer>
			<WalletInfoContainer>
				<IconContainer>
					<MetamaskIcon width={18} />
				</IconContainer>
				<UserInfoContainer>
					<UserInfoCoinContainer>
						<Typography variant={'h6'}>
							{getFullDisplayBalance(balance, 18, 5)} BNB
						</Typography>
					</UserInfoCoinContainer>
					<UserInfoWalletAddressContainer>
						<UserInfoWalletAddressHeadContainer>
							<Typography variant={'s1'} color={'textBody'}>
								{props.account}
							</Typography>
						</UserInfoWalletAddressHeadContainer>
						<UserInfoWalletAddressEndContainer>
							<Typography variant={'s1'} color={'textBody'}>
								{props.account.slice(-6)}
							</Typography>
						</UserInfoWalletAddressEndContainer>
					</UserInfoWalletAddressContainer>
				</UserInfoContainer>
			</WalletInfoContainer>
			<UserAvatarContainer>
				<Image
					alt={'UserAvatar'}
					height={48}
					src={getPublicImageResource('user-avatar.png')}
					width={48}
				/>
			</UserAvatarContainer>
		</WalletSectionContainer>
	);
}

export default WalletSection;
