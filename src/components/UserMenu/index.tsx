import { useWeb3React } from '@web3-react/core';
import Row, { AutoRow } from 'components/Layout/Row';
import styled from 'styled-components';
import { Flex } from 'ui-kit/components/Box';
import { DagaTheme } from 'ui-kit/theme';
import { majorScale } from 'ui-kit/utils/spacingRule/majorScale';
import { majorScalePx } from 'ui-kit/utils/spacingRule/majorScalePx';
import { DashboardIcon, MarketplaceIcon } from 'ui-kit/components/Icon';
import { PaddingXResponsive } from 'components/utils/constant';
import SectionLink from 'components/SectionLink';
import { Typography } from 'ui-kit/components/Typography';
import { LayoutCenter } from 'ui-kit/utils';
import LogoLink from './LogoLink';
import WalletSection from './WalletSection';

export const UserMenuContainer = styled(Flex)`
	background-color: ${({ theme }: { theme: DagaTheme }) =>
		`${theme.colors.backgroundStandard}`};
	border-bottom: 1px solid rgba(255, 255, 255, 0.06);
	box-sizing: border-box;
	height: ${majorScalePx(18)};
	padding-top: ${majorScalePx(3)};
	padding-bottom: ${majorScalePx(3)};

	.content {
		justify-content: space-between;
		width: 100%;
	}

	${PaddingXResponsive}
`;

export const LeftSideContainer = styled.div`
	display: flex;
`;

export const UserMenuSubItemContainer = styled.div`
	${LayoutCenter};

	padding-left: ${majorScalePx(3)};
	padding-right: ${majorScalePx(3)};
`;

function UserMenu() {
	const { account } = useWeb3React();
	return (
		<UserMenuContainer
			flexDirection="row"
			alignItems="center"
			justifyContent="center"
			py={majorScale(4)}
		>
			<Row className="content">
				<LeftSideContainer>
					<Typography variant={'s1'} marginRight={'40px'}>
						<LogoLink />
					</Typography>
					<UserMenuSubItemContainer>
						<SectionLink
							IconComponent={<MarketplaceIcon width={'13px'} />}
							href="/marketplace"
							displayName="Marketplace"
							variant={'h5'}
						/>
					</UserMenuSubItemContainer>
					<UserMenuSubItemContainer>
						<SectionLink
							IconComponent={<DashboardIcon width={'12px'} />}
							href="/dashboard"
							displayName="Dashboard"
							variant={'h5'}
						/>
					</UserMenuSubItemContainer>
				</LeftSideContainer>

				<WalletSection account={account} />
			</Row>
		</UserMenuContainer>
	);
}

export default UserMenu;
