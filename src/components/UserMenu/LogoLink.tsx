import React from 'react';
import styled from 'styled-components';
import { getPublicImageResource } from 'utils/getResource';

const Image = styled.img`
	height: 100%;
`;

function LogoLink() {
	const landingPageUrl = process.env.REACT_APP_LANDING_PAGE_URL;
	return (
		<a href={landingPageUrl}>
			<Image alt={'Logo'} src={getPublicImageResource('logo.svg')} />
		</a>
	);
}

export default LogoLink;
