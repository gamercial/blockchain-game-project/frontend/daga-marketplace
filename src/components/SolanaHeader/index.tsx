import React from 'react';
import {
	LeftSideContainer,
	UserMenuContainer,
	UserMenuSubItemContainer,
} from 'components/UserMenu';
import Row, { AutoRow } from 'components/Layout/Row';
import { DagaTheme } from 'ui-kit/theme';
import { majorScale } from 'ui-kit/utils/spacingRule/majorScale';
import { majorScalePx } from 'ui-kit/utils/spacingRule/majorScalePx';
import { DashboardIcon, MarketplaceIcon } from 'ui-kit/components/Icon';
import { PaddingXResponsive } from 'components/utils/constant';
import SectionLink from 'components/SectionLink';
import { Typography } from 'ui-kit/components/Typography';
import { LayoutCenter } from 'ui-kit/utils';
import LogoLink from 'components/UserMenu/LogoLink';
import SolanaWalletSection from './SolanaWalletSection';
import { useWallet } from '@solana/wallet-adapter-react';
import { WalletMultiButton } from '@solana/wallet-adapter-ant-design';
import { Solana } from 'utils/pageDirectionConstant';

function SolanaHeader() {
	const wallet = useWallet();
	const { connected, adapter } = wallet;
	return (
		<UserMenuContainer
			flexDirection="row"
			alignItems="center"
			justifyContent="center"
			py={majorScale(4)}
		>
			<Row className="content">
				<LeftSideContainer>
					<Typography variant={'s1'} marginRight={'40px'}>
						<LogoLink />
					</Typography>
					<UserMenuSubItemContainer>
						<SectionLink
							IconComponent={<MarketplaceIcon width={'13px'} />}
							href={Solana.Marketplace.Root}
							displayName="Marketplace"
							variant={'h5'}
						/>
					</UserMenuSubItemContainer>
					<UserMenuSubItemContainer>
						<SectionLink
							IconComponent={<DashboardIcon width={'12px'} />}
							href={Solana.Dashboard.Root}
							displayName="Dashboard"
							variant={'h5'}
						/>
					</UserMenuSubItemContainer>
				</LeftSideContainer>
				{connected ? (
					<SolanaWalletSection account={adapter?.publicKey?.toBase58()} />
				) : (
					<WalletMultiButton type="primary" />
				)}
			</Row>
		</UserMenuContainer>
	);
}

export default SolanaHeader;
