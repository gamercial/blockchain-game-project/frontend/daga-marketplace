import React from 'react';
import {
	IconContainer,
	UserAvatarContainer,
	UserInfoCoinContainer,
	UserInfoContainer,
	UserInfoWalletAddressContainer,
	UserInfoWalletAddressEndContainer,
	UserInfoWalletAddressHeadContainer,
	WalletInfoContainer,
	WalletSectionContainer,
} from 'components/UserMenu/WalletSection';
import { MetamaskIcon } from 'ui-kit/components/Icon';
import { getFullDisplayBalance } from 'utils/formatBalance/getFullDisplayBalance';
import { Typography } from 'ui-kit/components/Typography';
import { Image } from 'ui-kit/components/Image';
import { useSolanaBalance } from 'hooks/useSolanaBalance';
import { getPublicImageResource } from 'utils/getResource';
import { WalletMultiButton } from '@solana/wallet-adapter-ant-design';
import { Box, Flex } from 'ui-kit/components/Box';
import { majorScale, majorScalePx } from 'ui-kit/utils';

function SolanaWalletSection(props: { account?: string }) {
	const solBalance = useSolanaBalance();
	if (!props.account) {
		return null;
	}
	return (
		<Flex display="flex" flexDirection="row" alignItems="center">
			<Flex marginRight={majorScalePx(2)}>
				<WalletMultiButton type="primary" />
			</Flex>
			<UserAvatarContainer>
				<Image
					alt={'UserAvatar'}
					height={48}
					src={getPublicImageResource('user-avatar.png')}
					width={48}
				/>
			</UserAvatarContainer>
		</Flex>
	);
}

export default SolanaWalletSection;
