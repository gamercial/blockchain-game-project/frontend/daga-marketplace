import styled from 'styled-components';
import {
	ComposedProps,
	composedStyles,
	majorScalePx,
	LayoutCenter,
} from 'ui-kit/utils';

export const CountdownContainer = styled.div<{ fontSize: number }>`
	font-size: ${({ fontSize }) => `${fontSize}px`};
`;

export const PreorderCountdownContainer = styled.div<ComposedProps>`
	background: ${({ theme }) => theme.colors.backgroundLighter};
	border: 1px solid ${({ theme }) => theme.colors.backgroundStroke};
	border-radius: 12px;
	box-sizing: border-box;
	min-width: 248px;
	padding: ${majorScalePx(4)};
	width: 100%;
	${composedStyles}
`;

export const PreorderCountdownTitleContainer = styled.div`
	padding-bottom: ${majorScalePx(3)};
	${LayoutCenter}
`;

export const PreorderCountdownTimerContainer = styled.div``;

export const PreorderCountdownTimerNumber = styled.div`
	${LayoutCenter};
	margin-bottom: ${majorScalePx(1)};
`;

export const PreorderCountdownTimerTitle = styled.div`
	${LayoutCenter}
`;
