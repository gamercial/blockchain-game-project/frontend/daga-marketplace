import { LayoutProps } from 'styled-system';
import { Grid } from 'ui-kit/components/Box';
import { Typography } from 'ui-kit/components/Typography';
import {
	CountdownDateRenderer,
	CountdownProps,
	PreorderCountdownProps,
	PreorderCountdownTimerProps,
} from './types';
import Countdown from './Countdown';
import {
	PreorderCountdownContainer,
	PreorderCountdownTimerContainer,
	PreorderCountdownTimerNumber,
	PreorderCountdownTimerTitle,
} from './styles';

function PreorderCountdownTimer(props: PreorderCountdownTimerProps) {
	return (
		<PreorderCountdownTimerContainer>
			<PreorderCountdownTimerNumber>
				<Typography color={'textTitle'} variant={'h5'}>
					{props.timer}
				</Typography>
			</PreorderCountdownTimerNumber>
			<PreorderCountdownTimerTitle>
				<Typography color={'textBody'} variant={'s3'}>
					{props.title}
				</Typography>
			</PreorderCountdownTimerTitle>
		</PreorderCountdownTimerContainer>
	);
}

function PreorderCountdownComp(props: PreorderCountdownProps & LayoutProps) {
	const { day, hours, minutes, seconds } = props;
	return (
		<PreorderCountdownContainer {...props}>
			<Grid
				justifyItems={'center'}
				alignContent={'center'}
				gridTemplateColumns={'1fr 1fr 1fr 1fr'}
			>
				<PreorderCountdownTimer timer={day} title={'Days'} />
				<PreorderCountdownTimer timer={hours} title={'Hours'} />
				<PreorderCountdownTimer timer={minutes} title={'Mins'} />
				<PreorderCountdownTimer timer={seconds} title={'Secs'} />
			</Grid>
		</PreorderCountdownContainer>
	);
}

function PreorderCountdown(props: CountdownProps & LayoutProps) {
	return (
		<Countdown endTime={props.endTime}>
			{({ time }: { time: CountdownDateRenderer }) => (
				<PreorderCountdownComp
					{...props}
					day={time.days}
					hours={time.hours}
					minutes={time.minutes}
					seconds={time.seconds}
				/>
			)}
		</Countdown>
	);
}

export default PreorderCountdown;
