import { css, DefaultTheme } from 'styled-components';
import { majorScalePx } from 'ui-kit/utils';

export const PaddingX = majorScalePx(39);

export const PaddingXResponsive = css`
	padding-left: ${majorScalePx(4)};
	padding-right: ${majorScalePx(4)};
	${({ theme }: { theme: DefaultTheme }) => theme.mediaQueries.sm} {
		padding-left: ${majorScalePx(6)};
		padding-right: ${majorScalePx(6)};
	}
`;
