import React from 'react';
import styled from 'styled-components';
import { Box } from 'ui-kit/components/Box';
import { CloseIcon } from 'ui-kit/components/Icon';
import { majorScalePx } from 'ui-kit/utils';
import { BaseProps } from './types';

export const BaseContainer = styled(Box)<{ minWidth: string }>`
	background: ${({ theme }) => theme.modal.background};
	border-radius: 24px;
	max-height: 100vh;
	overflow: hidden;
	position: relative;
	width: 100%;
	z-index: ${({ theme }) => theme.zIndices.modal};

	${({ theme }) => theme.mediaQueries.xs} {
		width: auto;
		min-width: ${({ minWidth }) => minWidth};
		max-width: 100%;
	}
`;

const CloseIconContainer = styled.div`
	cursor: pointer;
	position: absolute;
	right: ${majorScalePx(4)};
	top: ${majorScalePx(4)};
	> svg {
		fill: ${({ theme }) => theme.colors.textHolder};
	}
`;

const ContentContainer = styled.div`
	padding: ${majorScalePx(14)};
`;

const HeaderContainer = styled.div`
	margin-bottom: ${majorScalePx(2)};
`;

const BodyContainer = styled.div`
	margin-bottom: ${majorScalePx(10)};
`;

const FooterContainer = styled.div``;

function Base(props: BaseProps): JSX.Element {
	console.log(`KDebug 🚩 Base ~ Base`, props);
	return (
		<BaseContainer minWidth={majorScalePx(118)}>
			<CloseIconContainer onClick={props.onDismiss}>
				<CloseIcon width={16} />
			</CloseIconContainer>
			<ContentContainer>
				<HeaderContainer>{props.header}</HeaderContainer>
				<BodyContainer>{props.body}</BodyContainer>
				<FooterContainer>{props.footer}</FooterContainer>
			</ContentContainer>
		</BaseContainer>
	);
}

export default Base;
