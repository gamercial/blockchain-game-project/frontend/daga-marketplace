import React, { useState } from 'react';
import styled from 'styled-components';
import { upperCase } from 'lodash';
import { Typography } from 'ui-kit/components/Typography';
import { Button } from 'ui-kit/components/Button';
import { LayoutCenter, majorScalePx } from 'ui-kit/utils';
import Base from './Base';
import { SellProductModalProps } from './types';

const HeaderContainer = styled.div`
	${LayoutCenter};
	margin-bottom: ${majorScalePx(2)};
`;
function Header(props: { title: string }): JSX.Element {
	return (
		<HeaderContainer>
			<Typography variant={'h3'}>{props.title}</Typography>
		</HeaderContainer>
	);
}

const BodyContainer = styled.div``;
const SubTradingFeeWarningContainer = styled.div`
	${LayoutCenter};
	margin-bottom: ${majorScalePx(8)};
`;
const PriceSettingContainer = styled.div`
	${LayoutCenter};
`;
const PriceSettingTitle = styled.div`
	margin-right: ${majorScalePx(3)};
`;
const PriceSettingControl = styled.div`
	${LayoutCenter};
	background-color: ${({ theme }) => theme.colors.backgroundLighter};
	border: 1px solid ${({ theme }) => theme.colors.backgroundStroke};
	border-radius: 12px;
	box-sizing: border-box;
	justify-content: space-between;
	height: 48px;
	min-width: 130px;
	padding: ${majorScalePx(3)};
`;
const PriceSettingControlInput = styled.input.attrs({ type: 'text' })`
	background-color: ${({ theme }) => theme.colors.backgroundLighter};
	border: none;
	border-radius: 0;
	box-sizing: border-box;
	color: ${({ theme }) => theme.colors.textTitle};
	text-align: center;
	width: 70px;

	&:focus {
		outline: none;
	}
`;
const PriceSettingControlPriceUnit = styled.div`
	margin-left: ${majorScalePx(2)};
`;
function Body(props: {
	onChangePrice(price: string): void;
	priceUnit: string;
	subTradingFeeLink: string;
}): JSX.Element {
	const [price, setPrice] = useState('');
	const onForceChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
		const value: string = e.target.value;
		const deleteAllNonNumericChar: string = value.replace(/(?!\.)\D/g, '');
		const removeAllExtraDotsExceptFirstOne: string =
			deleteAllNonNumericChar.replace(/(?<=\..*)\./g, '');
		const keepDecimal: string = removeAllExtraDotsExceptFirstOne.replace(
			/(?<=\.\d\d\d).*/g,
			''
		);
		const insertCommas: string = keepDecimal.replace(
			/\B(?=(\d{3})+(?!\d))/g,
			','
		);
		setPrice(insertCommas);
		props.onChangePrice(insertCommas);
	};
	return (
		<BodyContainer>
			<SubTradingFeeWarningContainer>
				<Typography variant={'s2'} color={'textBody'}>
					Make sure you aware about
				</Typography>
				<a href={props.subTradingFeeLink}>
					<Typography variant={'s2'} color={'primaryBlueStandard'}>
						&nbsp;subtrading fee
					</Typography>
				</a>
			</SubTradingFeeWarningContainer>
			<PriceSettingContainer>
				<PriceSettingTitle>
					<Typography variant={'s2'} color={'textBody'}>
						Sell at :
					</Typography>
				</PriceSettingTitle>
				<PriceSettingControl>
					<PriceSettingControlInput
						onChange={onForceChange}
						placeholder={'Your price'}
						value={price}
					/>
					<PriceSettingControlPriceUnit>
						<Typography variant={'s2'} color={'textTitle'}>
							{upperCase(props.priceUnit)}
						</Typography>
					</PriceSettingControlPriceUnit>
				</PriceSettingControl>
			</PriceSettingContainer>
		</BodyContainer>
	);
}

const FooterContainer = styled.div`
	${LayoutCenter}
`;
function Footer(props: { onConfirm?(): void }): JSX.Element {
	const { onConfirm = () => null } = props;
	return (
		<FooterContainer>
			<Button onClick={onConfirm} width={'100%'}>
				Confirm
			</Button>
		</FooterContainer>
	);
}

function SellProductModal(props: SellProductModalProps): JSX.Element {
	const {
		onConfirm: onConfirmProps,
		onDismiss = () => null,
		priceUnit,
		subTradingFeeLink,
		title,
	} = props;
	const priceRef = React.useRef('');
	const onConfirm = (): void => {
		onConfirmProps(priceRef.current);
	};
	const onChangePrice = (price: string): void => {
		priceRef.current = price;
	};
	return (
		<Base
			header={<Header title={title} />}
			body={
				<Body
					onChangePrice={onChangePrice}
					priceUnit={priceUnit}
					subTradingFeeLink={subTradingFeeLink}
				/>
			}
			footer={<Footer onConfirm={onConfirm} />}
			onDismiss={onDismiss}
		/>
	);
}

export default SellProductModal;
