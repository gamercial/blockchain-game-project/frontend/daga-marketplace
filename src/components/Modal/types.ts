import { ReactNode } from 'react';

export type BaseProps = {
	header: ReactNode;
	body: ReactNode;
	footer: ReactNode;
	onDismiss(): void;
};

export type PurchaseSuccessfulModalProps = {
	onGoToActivity?(): void;
	onDismiss?(): void;
	purchasedProductName: string;
};

export type SellProductModalProps = {
	onConfirm(price: string): void;
	onDismiss?(): void;
	priceUnit: string;
	subTradingFeeLink: string;
	title: string;
};
