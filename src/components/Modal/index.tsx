import { useModal } from 'ui-kit';
import PurchaseSuccessfulModal from './PurchaseSuccessfulModal';
import SellProductModal from './SellProductModal';

type ReturnType = [() => void, () => void];

export function usePurchaseSuccessfulModal(props: {
	onGoToActivity?(): void;
	purchasedProductName: string;
}): ReturnType {
	return useModal(
		<PurchaseSuccessfulModal
			onGoToActivity={props.onGoToActivity}
			purchasedProductName={props.purchasedProductName}
		/>
	);
}

export function useSellProductModal(props: {
	onConfirm(price: string): void;
	priceUnit: string;
	subTradingFeeLink: string;
	title: string;
}): ReturnType {
	return useModal(
		<SellProductModal
			onConfirm={props.onConfirm}
			priceUnit={props.priceUnit}
			subTradingFeeLink={props.subTradingFeeLink}
			title={props.title}
		/>
	);
}
