import styled from 'styled-components';
import { Button } from 'ui-kit/components/Button';
import { Typography } from 'ui-kit/components/Typography';
import { LayoutCenter, majorScalePx } from 'ui-kit/utils';
import Base from './Base';
import { PurchaseSuccessfulModalProps } from './types';

const HeaderContainer = styled.div`
	${LayoutCenter}
`;
function Header(): JSX.Element {
	return (
		<HeaderContainer>
			<Typography variant={'h3'}>Purchase Successfully</Typography>
		</HeaderContainer>
	);
}

const BodyContainer = styled.div`
	${LayoutCenter}
`;
function Body(props: { purchasedProductName: string }): JSX.Element {
	return (
		<BodyContainer>
			<Typography variant={'s1'} color={'textBody'}>
				{props.purchasedProductName} has transferred to your inventory
			</Typography>
		</BodyContainer>
	);
}

const FooterContainer = styled.div`
	${LayoutCenter}
`;
function Footer(props: {
	onGoToActivity?(): void;
	onDismiss(): void;
}): JSX.Element {
	const { onGoToActivity = () => null, onDismiss } = props;
	return (
		<FooterContainer>
			<Button marginRight={majorScalePx(4)} onClick={onGoToActivity}>
				Go to my activity
			</Button>
			<Button variant={'secondary'} onClick={onDismiss}>
				Cancel
			</Button>
		</FooterContainer>
	);
}

function PurchaseSuccessfulModal(
	props: PurchaseSuccessfulModalProps
): JSX.Element {
	const { onDismiss = () => null } = props;
	return (
		<Base
			header={<Header />}
			body={<Body purchasedProductName={props.purchasedProductName} />}
			footer={
				<Footer onGoToActivity={props.onGoToActivity} onDismiss={onDismiss} />
			}
			onDismiss={onDismiss}
		/>
	);
}

export default PurchaseSuccessfulModal;
