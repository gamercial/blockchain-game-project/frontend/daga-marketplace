import PageLoader from 'components/Loader/PageLoader';
import { Route, Router, Switch } from 'react-router';
import SuspenseWithChunkErrorHandler from 'components/SuspenseWithChunkErrorHandler';
import useEagerConnect from 'hooks/useEagerConnect';
import history from 'routerHistory';
import Routers from './routers';
import GlobalStyle from './style/Global';
import HeaderRouters from 'header-routers';

// Use require instead of import, and order matters
require('antd/dist/antd.dark.less');
require('@solana/wallet-adapter-ant-design/styles.css');
require('@solana/wallet-adapter-react-ui/styles.css');

function App() {
	// useEagerConnect();

	return (
		<>
			<GlobalStyle />
			<Router history={history}>
				<SuspenseWithChunkErrorHandler fallback={<PageLoader />}>
					<Switch>
						{HeaderRouters.map((headerRouter) => {
							return (
								<Route key={headerRouter.path} {...headerRouter}>
									<headerRouter.children {...headerRouter?.childrenProps} />
								</Route>
							);
						})}
					</Switch>
					<Switch>
						{Routers.map((router) => (
							<Route key={router.path} {...router}>
								<router.children {...router?.childrenProps} />
							</Route>
						))}
					</Switch>
				</SuspenseWithChunkErrorHandler>
			</Router>
		</>
	);
}

export default App;
