import { lazy } from 'react';
import { Redirect } from 'react-router';
import { BSC, Solana } from 'utils/pageDirectionConstant';

export type RouterType = {
	exact: boolean;
	children: any;
	childrenProps?: any;
	path: string;
};

const RootRouter: RouterType = {
	path: '/',
	exact: true,
	children: Redirect,
	childrenProps: {
		to: Solana.Marketplace.Root,
	},
};

const MarketplaceItemRouter: RouterType = {
	path: BSC.Marketplace.Item,
	exact: true,
	children: lazy(() => import('./pages/MarketplacePages/PreorderPage')),
	childrenProps: {
		showEggItem: true,
	},
};

const PreorderRouter: RouterType = {
	path: BSC.Marketplace.Preorder,
	exact: true,
	children: lazy(() => import('./pages/MarketplacePages/PreorderPage')),
};

const RootMyDashboardRouter: RouterType = {
	path: BSC.Dashboard.Root,
	exact: true,
	children: lazy(() => import('./pages/MyDashboard')),
};

const MyDashboardItemRouter: RouterType = {
	path: BSC.Dashboard.Item,
	exact: true,
	children: lazy(() => import('./pages/MyDashboard')),
	childrenProps: {
		pathSegment: 'egg-item',
	},
};

const MarketplaceRootRouter: RouterType = {
	path: BSC.Marketplace.Root,
	exact: false,
	children: Redirect,
	childrenProps: {
		to: BSC.Marketplace.Preorder,
	},
};

/**
 * SOLANA
 */
const SolanaMarketplaceRootRouter: RouterType = {
	path: Solana.Marketplace.Root,
	exact: true,
	children: lazy(() => import('./pages/SolanaMarketplacePage')),
};

const SolanaMarketplaceItemRouter: RouterType = {
	path: Solana.Marketplace.Item,
	exact: true,
	children: lazy(() => import('./pages/MarketplacePages/PreorderPage')),
	childrenProps: {
		showEggItem: true,
	},
};

const SolanaDashboardRouter: RouterType = {
	path: Solana.Dashboard.Root,
	exact: true,
	children: lazy(() => import('./pages/SolanaDashboard')),
};

const Routers = [
	RootRouter,
	MarketplaceItemRouter,
	PreorderRouter,
	RootMyDashboardRouter,
	MyDashboardItemRouter,
	MarketplaceRootRouter,
	SolanaMarketplaceRootRouter,
	SolanaMarketplaceItemRouter,
	SolanaDashboardRouter,
];

export default Routers;
