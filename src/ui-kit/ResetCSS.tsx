import { createGlobalStyle } from 'styled-components';

const ResetCSS = createGlobalStyle`

  * {
    font-family: 'DM Sans', sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

`;

export default ResetCSS;
