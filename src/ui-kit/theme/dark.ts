import { DefaultTheme } from 'styled-components';
import { darkModal } from 'ui-kit/widgets/Modal/theme';
import base from './base';
import { darkColors } from './colors';

const darkTheme: DefaultTheme = {
	...base,
	modal: darkModal,
	isDark: true,
	colors: darkColors,
};

export default darkTheme;
