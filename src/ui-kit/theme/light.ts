import { DefaultTheme } from 'styled-components';
import { lightModal } from 'ui-kit/widgets/Modal/theme';
import base from './base';
import { lightColors } from './colors';

const lightTheme: DefaultTheme = {
	...base,
	modal: lightModal,
	isDark: false,
	colors: lightColors,
};

export default lightTheme;
