import {
	BackgroundColorsMixed,
	Colors,
	PrimaryColorsMixed,
	TextColorsMixed,
} from './types';

export const backgroundColors: BackgroundColorsMixed = {
	dark: {
		backgroundDarker: '#121315',
		backgroundStandard: '#202023',
		backgroundLighter: '#303036',
		backgroundStroke: '#44444A',
		backgroundSuccess: '#169231',
		backgroundError: '#A52727',
		// phantom wallet color
		backgroundSecondary: '#7E00D3',
	},
	light: {
		backgroundDarker: '#121315',
		backgroundStandard: '#202023',
		backgroundLighter: '#303036',
		backgroundStroke: '#44444A',
		backgroundSuccess: '#169231',
		backgroundError: '#A52727',
		// phantom wallet color
		backgroundSecondary: '#7E00D3',
	},
};

export const primaryColors: PrimaryColorsMixed = {
	dark: {
		primaryDarker: '#03C386',
		primaryStandard: '#11FFB3',
		primaryLighter: '#74FFD3',
		white: '#F8FAFE',
	},
	light: {
		primaryLighter: '#5A90E2',
		primaryDarker: '#1C59B6',
		primaryStandard: '#2C72DB',
		white: '#F8FAFE',
	},
};

export const textColors: TextColorsMixed = {
	dark: {
		textBody: '#A1A3A8',
		textHolder: '#787C82',
		textTitle: '#F8FAFE',
		textButton: '#1A1B1D',
	},
	light: {
		textBody: '#A1A3A8',
		textHolder: '#787C82',
		textTitle: '#F8FAFE',
		textButton: '#1A1B1D',
	},
};

export const lightColors: Colors = {
	...backgroundColors.light,
	...primaryColors.light,
	...textColors.light,
	overlay: 'rgba(0, 0, 0, 0.6)',
};

export const darkColors: Colors = {
	...backgroundColors.dark,
	...primaryColors.dark,
	...textColors.dark,
	overlay: 'rgba(0, 0, 0, 0.6)',
};
