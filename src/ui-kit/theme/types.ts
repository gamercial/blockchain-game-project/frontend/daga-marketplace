export type Breakpoints = string[];

export type MediaQueries = {
	xs: string;
	sm: string;
	md: string;
	lg: string;
	xl: string;
	xxl: string;
	nav: string;
};

export type Spacing = number[];

export type Radii = {
	small: string;
	default: string;
	card: string;
	circle: string;
};

export type Shadows = {
	level1: string;
	active: string;
	success: string;
	warning: string;
	focus: string;
	inset: string;
};

export type Gradients = {
	bubblegum: string;
	inverseBubblegum: string;
	cardHeader: string;
	blue: string;
	violet: string;
	violetAlt: string;
	gold: string;
};

export interface Colors extends BackgroundColors, PrimaryColors, TextColors {
	overlay: string;
}

export type BackgroundColorsMixed = {
	dark: BackgroundColors;
	light: BackgroundColors;
};

export type BackgroundColors = {
	backgroundDarker: string;
	backgroundError: string;
	backgroundLighter: string;
	backgroundStandard: string;
	backgroundStroke: string;
	backgroundSuccess: string;
	backgroundSecondary: string;
};

export type PrimaryColorsMixed = {
	dark: PrimaryColors;
	light: PrimaryColors;
};

export type PrimaryColors = {
	primaryDarker: string;
	primaryStandard: string;
	primaryLighter: string;
	white: string;
};

export type TextColorsMixed = {
	dark: TextColors;
	light: TextColors;
};

export type TextColors = {
	textBody: string;
	textHolder: string;
	textTitle: string;
	textButton: string;
};

export type ZIndices = {
	dropdown: number;
	modal: number;
};
