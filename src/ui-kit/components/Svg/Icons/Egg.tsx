import React from 'react';
import Svg from '../Svg';
import { SvgProps } from '../types';

function Egg(props: SvgProps) {
	return (
		<Svg
			width="24"
			height="24"
			viewBox="0 0 24 24"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
			{...props}
		>
			<g clipPath="url(#clip0)">
				<path
					d="M21 14C21 19.799 16.9706 23 12 23C7.02944 23 3 20.299 3 14.5C3 8.70101 7.52944 1.5 12.5 1.5C17.4706 1.5 21 8.20101 21 14Z"
					fill="#4B36A7"
				/>
				<path
					fillRule="evenodd"
					clipRule="evenodd"
					d="M15.9704 2.66958C15.955 2.73453 15.9344 2.81366 15.9105 2.9052C15.6334 3.96828 14.9193 6.70708 16.8833 8.40305C16.959 8.46841 17.0434 8.52516 17.1299 8.57526L20.6205 10.5961C20.8685 11.7279 20.9999 12.8808 20.9999 14C20.9999 18.9987 18.0059 22.067 13.9959 22.8187C13.9986 22.7864 13.9999 22.754 13.9999 22.7215V21.0704C13.9999 20.4017 13.6657 19.7772 13.1094 19.4063L12.4999 19L11.0382 17.9037C10.689 17.6418 10.435 17.2728 10.3151 16.8531L9.49995 14L7.69578 9.48957C7.56742 9.16867 7.35809 8.88652 7.08821 8.67061L6.20982 7.9679C5.81489 7.65195 5.3286 7.50567 4.85132 7.53398C6.6291 4.15238 9.48328 1.5 12.4999 1.5C13.7609 1.5 14.9291 1.93123 15.9704 2.66958Z"
					fill="#5C47B6"
				/>
				<path
					fillRule="evenodd"
					clipRule="evenodd"
					d="M12.0001 22.9998C16.9706 22.9998 21.0001 19.7988 21.0001 13.9998C21.0001 12.6179 20.7996 11.1847 20.427 9.80322C19.4534 18.5818 13.1865 21.9393 8.50464 22.4897C9.5793 22.8318 10.7604 22.9998 12.0001 22.9998Z"
					fill="white"
					fillOpacity="0.1"
				/>
				<g filter="url(#filter0_df)">
					<path
						d="M6.5 4L7.03115 6.86528L9.14503 4.85942L7.89058 7.48969L10.7798 7.10942L8.21885 8.5L10.7798 9.89058L7.89058 9.51031L9.14503 12.1406L7.03115 10.1347L6.5 13L5.96885 10.1347L3.85497 12.1406L5.10942 9.51031L2.22025 9.89058L4.78115 8.5L2.22025 7.10942L5.10942 7.48969L3.85497 4.85942L5.96885 6.86528L6.5 4Z"
						fill="white"
						fillOpacity="0.52"
						shapeRendering="crispEdges"
					/>
				</g>
				<g filter="url(#filter1_df)">
					<path
						d="M6.5 6L6.79508 7.59182L7.96946 6.47746L7.27254 7.93871L8.87764 7.72746L7.45492 8.5L8.87764 9.27254L7.27254 9.06129L7.96946 10.5225L6.79508 9.40818L6.5 11L6.20492 9.40818L5.03054 10.5225L5.72746 9.06129L4.12236 9.27254L5.54508 8.5L4.12236 7.72746L5.72746 7.93871L5.03054 6.47746L6.20492 7.59182L6.5 6Z"
						fill="white"
						fillOpacity="0.52"
						shapeRendering="crispEdges"
					/>
				</g>
			</g>
			<defs>
				<filter
					id="filter0_df"
					x="-1.77979"
					y="0"
					width="16.5595"
					height="17"
					filterUnits="userSpaceOnUse"
					colorInterpolationFilters="sRGB"
				>
					<feFlood floodOpacity="0" result="BackgroundImageFix" />
					<feColorMatrix
						in="SourceAlpha"
						type="matrix"
						values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
						result="hardAlpha"
					/>
					<feOffset />
					<feGaussianBlur stdDeviation="2" />
					<feComposite in2="hardAlpha" operator="out" />
					<feColorMatrix
						type="matrix"
						values="0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 1 0"
					/>
					<feBlend
						mode="normal"
						in2="BackgroundImageFix"
						result="effect1_dropShadow"
					/>
					<feBlend
						mode="normal"
						in="SourceGraphic"
						in2="effect1_dropShadow"
						result="shape"
					/>
					<feGaussianBlur stdDeviation="0.3" result="effect2_foregroundBlur" />
				</filter>
				<filter
					id="filter1_df"
					x="0.122314"
					y="2"
					width="12.7553"
					height="13"
					filterUnits="userSpaceOnUse"
					colorInterpolationFilters="sRGB"
				>
					<feFlood floodOpacity="0" result="BackgroundImageFix" />
					<feColorMatrix
						in="SourceAlpha"
						type="matrix"
						values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
						result="hardAlpha"
					/>
					<feOffset />
					<feGaussianBlur stdDeviation="2" />
					<feComposite in2="hardAlpha" operator="out" />
					<feColorMatrix
						type="matrix"
						values="0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 1 0"
					/>
					<feBlend
						mode="normal"
						in2="BackgroundImageFix"
						result="effect1_dropShadow"
					/>
					<feBlend
						mode="normal"
						in="SourceGraphic"
						in2="effect1_dropShadow"
						result="shape"
					/>
					<feGaussianBlur stdDeviation="0.3" result="effect2_foregroundBlur" />
				</filter>
				<clipPath id="clip0">
					<rect width="24" height="24" fill="white" />
				</clipPath>
			</defs>
		</Svg>
	);
}

export default Egg;
