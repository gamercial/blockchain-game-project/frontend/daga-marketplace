import styled from 'styled-components';
import { Typography } from 'ui-kit/components/Typography';
import { NonSelected } from '../../utils';

export const SliderContainer = styled.div`
	height: 12px;
	position: relative;
	width: 100%;
`;

export const Rail = styled.div`
	background-color: ${({ theme }) => theme.colors.backgroundStroke};
	height: 1px;
	position: absolute;
	top: 50%;
	width: 100%;
`;

export const Track = styled.div<{ percentage: number }>`
	background-color: ${({ theme }) => theme.colors.primaryStandard};
	height: 1px;
	left: 0;
	position: absolute;
	top: 50%;
	width: ${({ percentage }) => (percentage != null ? `${percentage}%` : 0)};
`;

export const Mark = styled.div<{
	active: boolean;
	left: number;
	isLast: boolean;
}>`
	background-color: ${({ active, theme }) =>
		active ? theme.colors.primaryStandard : theme.colors.backgroundStroke};
	cursor: pointer;
	height: 8px;
	left: ${({ left }) => (left != null ? `${left}%` : 0)};
	position: absolute;
	top: 20%;
	transform: ${({ isLast }) => (isLast ? `translateX(-1px)` : '')};
	width: 1px;
`;

export const Thumb = styled.div<{ percentage: number }>`
	background-color: ${({ theme }) => theme.colors.backgroundLighter};
	border: 2px solid ${({ theme }) => theme.colors.primaryStandard};
	border-radius: 50%;
	box-sizing: border-box;
	height: 100%;
	left: ${({ percentage }) => (percentage != null ? `${percentage}%` : 0)};
	position: absolute;
	transform: translateX(-50%);
	width: 12px;

	&:hover {
		cursor: pointer;
	}
`;

const StepCounterContainer = styled.div<{
	active: boolean;
	left: number;
	isLast: boolean;
}>`
	cursor: pointer;
	left: ${({ left }) => (left != null ? `${left}%` : 0)};
	position: absolute;
	top: 100%;
	transform: translateX(-50%);
	${NonSelected};
`;

export function StepCounter(props: any): JSX.Element {
	return (
		<StepCounterContainer {...props}>
			<Typography
				variant={'h6'}
				color={props.active ? 'primaryStandard' : 'textHolder'}
			>
				{props.value}
			</Typography>
		</StepCounterContainer>
	);
}
