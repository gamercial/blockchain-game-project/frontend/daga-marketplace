import Slider, { SliderProps } from '.';

const index = {
	title: 'Components/Slider',
	component: Slider,
	argTypes: {},
};
export default index;

export const Default = (props: SliderProps) => {
	return <Slider {...props} />;
};
