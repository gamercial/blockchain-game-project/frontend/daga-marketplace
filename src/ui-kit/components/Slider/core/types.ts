import { ElementType } from 'react';

export type SliderBaseProps = {
	components: {
		Container: ElementType;
		Rail: ElementType;
		Mark: ElementType;
		Thumb: ElementType;
		Track: ElementType;
		StepCounter?: ElementType;
	};
	defaultValue?: number;
	min: number;
	max: number;
	step: number;
};
