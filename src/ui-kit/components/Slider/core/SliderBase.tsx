import React from 'react';
import { ownerDocument, useEventCallback } from 'ui-kit/utils';
import { SliderBaseProps } from './types';

function getOffset(event: any): { x: number; y: number } {
	return {
		x: event.clientX,
		y: event.clientY,
	};
}

function valueToPercent(value: number, min: number, max: number): number {
	return ((value - min) * 100) / (max - min);
}

function percentToValue(percent: number, min: number, max: number): number {
	return (max - min) * percent + min;
}

function roundValueToStep(value: number, step: number, min: number): number {
	return Math.round((value - min) / step) * step + min;
}

function stepToPercent(step: number, min: number, max: number): number {
	return Math.round((step / (max - min)) * 100);
}

function getValueState({
	nodeRef,
	offset,
	min,
	max,
	step,
}: {
	nodeRef: any;
	offset: { x: number; y: number };
	min: number;
	max: number;
	step: number;
}): { step: number; percent: number } {
	const { width, left } = nodeRef?.current?.getBoundingClientRect?.();
	const percent = (offset.x - left) / width;
	const newValue = percentToValue(percent, min, max);
	const newStep = roundValueToStep(newValue, step, min);
	const value = stepToPercent(newStep, min, max);
	return {
		step: newStep,
		percent: value,
	};
}

function SliderBase(props: SliderBaseProps): JSX.Element {
	const {
		components: {
			Container: ContainerProps,
			Rail: RailProps,
			Mark: MarkProps,
			Thumb: ThumbProps,
			Track: TrackProps,
			StepCounter,
		},
		defaultValue,
		min,
		max,
		step,
	} = props;
	const [values, setValueState] = React.useState({
		step: defaultValue ?? min,
		percent: 0, // Get default percent
	});

	const sliderRef = React.useRef();
	const handleMouseDown = useEventCallback((event: any) => {
		event.preventDefault();
		const newValueState = getValueState({
			nodeRef: sliderRef,
			offset: getOffset(event),
			min,
			max,
			step,
		});
		setValueState(newValueState);
		const doc = ownerDocument(sliderRef.current);
		doc.addEventListener('mousemove', handleTouchMove);
		doc.addEventListener('mouseup', handleTouchEnd);
	});

	const handleTouchMove = useEventCallback(
		(nativeEvent: MouseEvent | TouchEvent) => {
			nativeEvent.preventDefault();
			const newValueState = getValueState({
				nodeRef: sliderRef,
				offset: getOffset(nativeEvent),
				min,
				max,
				step,
			});
			setValueState(newValueState);
		}
	);

	const handleTouchEnd = useEventCallback(
		(nativeEvent: MouseEvent | TouchEvent) => {
			stopListening();
		}
	);

	const stopListening = React.useCallback(() => {
		const doc = ownerDocument(sliderRef.current);
		doc.removeEventListener('mousemove', handleTouchMove);
		doc.removeEventListener('mouseup', handleTouchEnd);
		doc.removeEventListener('touchmove', handleTouchMove);
		doc.removeEventListener('touchend', handleTouchEnd);
	}, [handleTouchEnd, handleTouchMove]);

	const marks = [...Array(Math.floor((max - min) / step) + 1)].map(
		(_, index) => ({
			value: min + step * index,
		})
	);
	const Container = ContainerProps || 'div';
	const Rail = RailProps || 'div';
	const Mark = MarkProps || 'div';
	const Thumb = ThumbProps || 'div';
	const Track = TrackProps || 'div';

	return (
		<Container ref={sliderRef} onMouseDown={handleMouseDown}>
			<Rail />
			<Track percentage={values.percent} />
			{marks.map((mark, index) => {
				const isActive = index <= values.step - min;
				const percentage = valueToPercent(mark.value, min, max);
				const isLast = index === marks.length - 1;
				return <Mark active={isActive} left={percentage} isLast={isLast} />;
			})}
			{StepCounter
				? marks.map((mark, index) => {
						const isActive = index === values.step;
						const percentage = valueToPercent(mark.value, min, max);
						const isLast = index === marks.length - 1;
						return (
							<StepCounter
								active={isActive}
								left={percentage}
								isLast={isLast}
								value={index}
							/>
						);
				  })
				: null}
			<Thumb percentage={values.percent} />
		</Container>
	);
}

export default SliderBase;
