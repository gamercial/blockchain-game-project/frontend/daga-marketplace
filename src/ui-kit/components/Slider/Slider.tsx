import { SliderBase } from './core';
import {
	SliderContainer,
	Mark,
	Rail,
	StepCounter,
	Thumb,
	Track,
} from './styles';
import { SliderProps } from './types';

function Slider(props: SliderProps): JSX.Element {
	const { min, max, step } = props;
	return (
		<SliderBase
			components={{
				Container: SliderContainer,
				Rail: Rail,
				Track: Track,
				Thumb: Thumb,
				Mark: Mark,
				StepCounter: StepCounter,
			}}
			min={min}
			max={max}
			step={step}
		/>
	);
}

Slider.defaultProps = {
	disabled: false,
	min: 0,
	max: 10,
	step: 1,
	marks: true,
	valueLabelDisplay: 'auto',
};
export default Slider;
