export type SliderProps = {
	defaultValue?: number;
	disabled: boolean;
	min: number;
	max: number;
	marks: boolean;
	onChange?(newValue: number, oldValue: number): void;
	step: number;
	valueLabelDisplay: 'auto' | 'on' | 'off';
};
