import { Typography } from 'ui-kit/components/Typography';
import { majorScalePx } from 'ui-kit/utils';
import { EggCardPriceIconContainer, EggCardPriceLabel } from '../styles';
import { EggCardPriceProps } from '../types';

function getEggCardPriceRenderer(props: EggCardPriceProps): JSX.Element {
	if (props.priceComponent) {
		return props.priceComponent;
	}
	return (
		<>
			<Typography
				color={'textBody'}
				mr={props.priceIcon ? majorScalePx(2) : '0px'}
				variant={'s1'}
			>
				{props.price}
			</Typography>
			<EggCardPriceIconContainer
				height={props.priceIcon ? majorScalePx(4) : '0px'}
				width={props.priceIcon ? majorScalePx(4) : '0px'}
			>
				{props.priceIcon}
			</EggCardPriceIconContainer>
		</>
	);
}

function EggCardPrice(props: EggCardPriceProps) {
	const children = getEggCardPriceRenderer(props);
	return <EggCardPriceLabel {...props}>{children}</EggCardPriceLabel>;
}

export default EggCardPrice;
