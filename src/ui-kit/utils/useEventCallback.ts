import * as React from 'react';
const useEnhancedEffect =
	typeof window !== 'undefined' ? React.useLayoutEffect : React.useEffect;

/**
 * See at: https://github.com/mui-org/material-ui/blob/master/packages/mui-utils/src/useEventCallback.ts
 * https://github.com/facebook/react/issues/14099#issuecomment-440013892
 */
export default function useEventCallback<Args extends unknown[], Return>(
	fn: (...args: Args) => Return
): (...args: Args) => Return {
	const ref = React.useRef(fn);
	useEnhancedEffect(() => {
		ref.current = fn;
	});
	return React.useCallback(
		(...args: Args) =>
			// @ts-expect-error hide `this`
			// tslint:disable-next-line:ban-comma-operator
			(0, ref.current!)(...args),
		[]
	);
}
