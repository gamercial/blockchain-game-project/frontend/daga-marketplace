import React, { useContext, useEffect, useMemo, useState } from 'react';
import { useLocalStorageState } from 'hooks/useLocalStorageState';
import { Keypair, clusterApiUrl, Connection } from '@solana/web3.js';
import {
	TokenListProvider,
	ENV as ChainID,
	TokenInfo,
} from '@solana/spl-token-registry';

export type ENV = 'mainnet-beta' | 'testnet' | 'devnet' | 'localnet';

export const ENDPOINTS = [
	{
		name: 'mainnet-beta' as ENV,
		endpoint: 'https://solana-api.projectserum.com/',
		chainID: ChainID.MainnetBeta,
	},
	{
		name: 'testnet' as ENV,
		endpoint: clusterApiUrl('testnet'),
		chainID: ChainID.Testnet,
	},
	{
		name: 'devnet' as ENV,
		endpoint: clusterApiUrl('devnet'),
		chainID: ChainID.Devnet,
	},
	{
		name: 'localnet' as ENV,
		endpoint: 'http://127.0.0.1:8899',
		chainID: ChainID.Devnet,
	},
];

const DEFAULT = ENDPOINTS[2].endpoint;
const DEFAULT_SLIPPAGE = 0.25;

interface ConnectionConfig {
	connection: Connection;
	sendConnection: Connection;
	endpoint: string;
	slippage: number;
	setSlippage: (val: number) => void;
	env: ENV;
	setEndpoint: (val: string) => void;
	tokens: TokenInfo[];
	tokenMap: Map<string, TokenInfo>;
}

const ConnectionContext = React.createContext<ConnectionConfig>({
	endpoint: DEFAULT,
	setEndpoint: () => {},
	slippage: DEFAULT_SLIPPAGE,
	setSlippage: (val: number) => {},
	connection: new Connection(DEFAULT, 'recent'),
	sendConnection: new Connection(DEFAULT, 'recent'),
	env: ENDPOINTS[0].name,
	tokens: [],
	tokenMap: new Map<string, TokenInfo>(),
});

export function useConnection(): Connection {
	return useContext(ConnectionContext).connection as Connection;
}

export function SolanaConnectionProvider({ children = undefined as any }) {
	const [endpoint, setEndpoint] = useLocalStorageState(
		'connectionEndpts',
		ENDPOINTS[0].endpoint
	);

	const [slippage, setSlippage] = useLocalStorageState(
		'slippage',
		DEFAULT_SLIPPAGE.toString()
	);

	const connection = useMemo(() => new Connection(DEFAULT, 'recent'), []);
	const sendConnection = useMemo(
		() => new Connection(endpoint, 'recent'),
		[endpoint]
	);

	const chain =
		ENDPOINTS.find((end) => end.endpoint === endpoint) || ENDPOINTS[0];
	const env = chain.name;

	const [tokens, setTokens] = useState<TokenInfo[]>([]);
	const [tokenMap, setTokenMap] = useState<Map<string, TokenInfo>>(new Map());
	useEffect(() => {
		// fetch token files
		(async () => {
			const res = await new TokenListProvider().resolve();
			const list = res
				.filterByChainId(chain.chainID)
				.excludeByTag('nft')
				.getList();
			const knownMints = list.reduce((map, item) => {
				map.set(item.address, item);
				return map;
			}, new Map<string, TokenInfo>());
			setTokenMap(knownMints);
			setTokens(list);
		})();
	}, [connection, chain]);

	// The websocket library solana/web3.js uses closes its websocket connection when the subscription list
	// is empty after opening its first time, preventing subsequent subscriptions from receiving responses.
	// This is a hack to prevent the list from every getting empty
	useEffect(() => {
		const id = connection.onAccountChange(new Keypair().publicKey, () => {});
		return () => {
			connection.removeAccountChangeListener(id);
		};
	}, [connection]);

	useEffect(() => {
		const id = connection.onSlotChange(() => null);
		return () => {
			connection.removeSlotChangeListener(id);
		};
	}, [connection]);

	useEffect(() => {
		const id = sendConnection.onAccountChange(
			new Keypair().publicKey,
			() => {}
		);
		return () => {
			sendConnection.removeAccountChangeListener(id);
		};
	}, [sendConnection]);

	useEffect(() => {
		const id = sendConnection.onSlotChange(() => null);
		return () => {
			sendConnection.removeSlotChangeListener(id);
		};
	}, [sendConnection]);

	return (
		<ConnectionContext.Provider
			value={{
				endpoint,
				setEndpoint,
				slippage: parseFloat(slippage),
				setSlippage: (val) => setSlippage(val.toString()),
				connection,
				sendConnection,
				tokens,
				tokenMap,
				env,
			}}
		>
			{children}
		</ConnectionContext.Provider>
	);
}
