import { Contract } from 'ethers';
import { useMemo } from 'react';
import { getDagaEggAddress } from 'utils/addressHelper';
import { getDagaSystemInfoContract } from 'utils/contractHelpers';
import { getContract } from 'utils/getContract';
import dagaEggAbi from 'configs/abi/DagaEgg.json';
import useActiveWeb3React from './useActiveWeb3React';

// returns null on errors
export function useContract(
	address: string | undefined,
	ABI: any,
	withSignerIfPossible = true
): Contract | null {
	const { library, account } = useActiveWeb3React();

	return useMemo(() => {
		if (!address || !ABI || !library) return null;
		try {
			return getContract(
				address,
				ABI,
				library,
				withSignerIfPossible && account ? account : undefined
			);
		} catch (error) {
			console.error('Failed to get contract', error);
			return null;
		}
	}, [address, ABI, library, withSignerIfPossible, account]);
}

export const useDagaSystemInfoContract = () => {
	return useMemo(() => getDagaSystemInfoContract(), []);
};

export const useDagaEggContract = (
	withSignerIfPossible?: boolean
): Contract | null => {
	return useContract(getDagaEggAddress(), dagaEggAbi, withSignerIfPossible);
};
