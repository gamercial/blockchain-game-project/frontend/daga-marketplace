import { useWallet } from '@solana/wallet-adapter-react';
import { useConnection } from 'contexts/solana-connection';
import { useEffect, useState } from 'react';

export function useSolanaBalance() {
	const { adapter } = useWallet();
	const [solBalance, setSolBalance] = useState(0);
	const connection = useConnection();
	useEffect(() => {
		if (!adapter || !adapter.publicKey) {
			setSolBalance(0);
		} else {
			connection.getBalance(adapter?.publicKey).then((balance) => {
				console.log(
					'🚀 ~ file: index.tsx ~ line 20 ~ connection.getBalance ~ balance',
					balance
				);
			});
			connection.getBalance(adapter.publicKey).then((balance) => {
				console.log(
					'🚀 ~ file: useSolanaBalance.ts ~ line 14 ~ connection.getBalance ~ balance',
					balance
				);
				setSolBalance(balance);
			});
		}
	}, [adapter, adapter?.publicKey, connection]);
	return solBalance;
}
