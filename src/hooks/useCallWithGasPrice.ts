import { useCallback } from 'react';
import ethers, { Contract, CallOverrides } from 'ethers';
import { get } from 'lodash';
import { GAS_PRICE_GWEI } from 'configs/constants/gasPrice';

/**
 * Perform a contract call with a gas price returned from useGasPrice
 * @param contract Used to perform the call
 * @param methodName The name of the method called
 * @param methodArgs An array of arguments to pass to the method
 * @param overrides An overrides object to pass to the method. gasPrice passed in here will take priority over the price returned by useGasPrice
 * @returns https://docs.ethers.io/v5/api/providers/types/#providers-TransactionReceipt
 */
export function useCallWithGasPrice() {
	const gasPrice = GAS_PRICE_GWEI.testnet;

	const callWithGasPrice = useCallback(
		async (
			contract: Contract,
			methodName: string,
			methodArgs: any[] = [],
			overrides?: CallOverrides
		): Promise<ethers.providers.TransactionResponse> => {
			console.log(
				'🚀 ~ file: useCallWithGasPrice.ts ~ line 24 ~ useCallWithGasPrice ~ overrides',
				overrides
			);
			const contractMethod = get(contract, methodName);
			const hasManualGasPriceOverride = overrides?.gasPrice;

			let tx;
			try {
				tx = await contractMethod(
					...methodArgs,
					hasManualGasPriceOverride
						? { ...overrides }
						: { ...overrides, gasPrice }
				);
			} catch (error) {
				console.log(`Try to contract has failed!`, error);
			}

			return tx;
		},
		[gasPrice]
	);

	return { callWithGasPrice };
}
