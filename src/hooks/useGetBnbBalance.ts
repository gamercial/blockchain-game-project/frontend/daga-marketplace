import { useWeb3React } from '@web3-react/core';
import BigNumber from 'bignumber.js';
import useLastUpdated from 'hooks/useLastUpdated';
import { useEffect, useState } from 'react';
import { BIG_ZERO } from 'utils/bigNumber';
import { simpleRpcProvider } from 'utils/providers';
import { FetchStatus } from './types/FetchStatus.model';

export const useGetBnbBalance = () => {
	const [fetchStatus, setFetchStatus] = useState(FetchStatus.NOT_FETCHED);
	const [balance, setBalance] = useState(BIG_ZERO);
	const { account } = useWeb3React();
	const { lastUpdated, setLastUpdated } = useLastUpdated();

	useEffect(() => {
		const fetchBalance = async () => {
			try {
				const walletBalance = await simpleRpcProvider.getBalance(
					account as string
				);
				setBalance(new BigNumber(walletBalance.toString()));
				setFetchStatus(FetchStatus.SUCCESS);
			} catch {
				setFetchStatus(FetchStatus.FAILED);
			}
		};

		if (account) {
			fetchBalance();
		}
	}, [account, lastUpdated, setBalance, setFetchStatus]);

	return { balance, fetchStatus, refresh: setLastUpdated };
};
