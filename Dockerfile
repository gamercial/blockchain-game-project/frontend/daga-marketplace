# pull official base image
FROM node:14.17.4-alpine3.11 as build

ARG REACT_APP_CHAIN_ID
ENV REACT_APP_CHAIN_ID=$REACT_APP_CHAIN_ID
ARG REACT_APP_LANDING_PAGE_URL
ENV REACT_APP_LANDING_PAGE_URL=$REACT_APP_LANDING_PAGE_URL
ARG REACT_APP_RPC_NODE_1
ENV REACT_APP_RPC_NODE_1=$REACT_APP_RPC_NODE_1
ARG REACT_APP_RPC_NODE_2
ENV REACT_APP_RPC_NODE_2=$REACT_APP_RPC_NODE_2
ARG REACT_APP_RPC_NODE_3
ENV REACT_APP_RPC_NODE_3=$REACT_APP_RPC_NODE_3

# install dependencies
RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh g++ make python

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install app dependencies
COPY package.json ./
COPY yarn.lock ./
RUN yarn install --silent

# add app
COPY . ./

RUN yarn run build

# Prepare nginx
FROM nginx:1.16.0-alpine
COPY --from=build /app/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d

# Fire up nginx
CMD ["nginx", "-g", "daemon off;"]
