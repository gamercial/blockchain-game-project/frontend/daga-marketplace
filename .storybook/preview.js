import { ModalProvider } from '../src/ui-kit/widgets/Modal';
import { withThemesProvider } from 'themeprovider-storybook';
import { light, dark } from '../src/ui-kit/theme';
import ResetCSS from '../src/ui-kit/ResetCSS';

export const parameters = {
	actions: { argTypesRegex: '^on[A-Z].*' },
	controls: {
		matchers: {
			color: /(background|color)$/i,
			date: /Date$/,
		},
	},
};

const themes = [
	{
		name: 'Dark',
		backgroundColor: dark.colors.background,
		...dark,
	},
	{
		name: 'Light',
		backgroundColor: light.colors.background,
		...light,
	},
];

const globalDecorator = (StoryFn) => (
	<ModalProvider>
		<ResetCSS />
		<StoryFn />
	</ModalProvider>
);

export const decorators = [globalDecorator, withThemesProvider(themes)];
